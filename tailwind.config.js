module.exports = {
  mode: 'jit',
  purge: [ 
  './resources/**/*.antlers.html',
  './resources/**/*.js',
  './resources/**/**/*.js',
  './resources/**/*.vue',
],

  darkMode: false, // or 'media' or 'class'

  
  theme: {
    extend: {

      screens: {
        'break1': '1312px',
        'break2': '1200px',
        'break3': '900px',
        'break4': '880px',
      },     
       
      fontSize: {
          'largestTitle': '5.625rem',            // 90px
          '6xl': '4.063rem',            // 65px
          'sixty': '3.75rem',            // 60px
          '5xl': '3.5rem',            // 56px
          'fifty': '3.125rem',            // 50px
          '4xl': '2.875rem',            // 46px
          '3xl': '2.25rem',            // 36px
          'thirty': '1.875rem',            // 30px
          'twentyEight': '1.65rem',            // 30px
          'twentysix': '1.625rem',            // 26px
          '2.4xl': '1.5rem',            // 24px
          '2xl': '1.25rem',            // 20px
          '1xl': '1.125rem',            // 18px
          'normal': '1rem',            // 16px
          '1xs': '0.875rem',            // 14px
          '2xs': '0.75rem',            // 12px
          '3xs': '0.688rem',            // 11px
      },        
      
    
      lineHeight: {   
        'ninety': '5.625rem',               // 90px
        'seventy': '4.375rem',               // 70px
        'sixtyfive': '4.063rem',               // 65px
        'fortysix': '2.875rem',               // 46px
        'thirtyfive': '2.188rem',               // 35px
        'thirty': '1.875rem',               // 30px
        'twentyeight': '1.75rem',               // 28px
        'twentyfive': '1.563rem',               // 25px

      },

      letterSpacing: {                
        widest: '.2em',
        widestBtn: '.16em',
        minus1: '-2px'
       },

       borderWidth: {
        DEFAULT: '1px',
        '0': '0',
        '2': '2px',
        '3': '3px',
        '4': '4px',
        '6': '6px',
        '8': '8px',
        '16': '16px',
        '18': '18px',
        '20': '20px',
      },

      rotate: {
        '10': '10deg',
        'neg10': '-10deg',
      },

    
  
      colors: {
        darkPurple:'#59235F',            // purple
        darkestPurple:'#471d4c',            // purple
        anotherPurple:'#7D3D85',            // purple
        seaGreen:'#015C71',            // green
        lightGreen:'#ddeef2',            // light green
        pinkyWhite:'#EEE9EF',            // pinky white
        darkerPinkyWhite:'#dfd5df',            // pinky white
        hoverOffWhite:'#faf9fa',            // off white
        
     
        spacing: {
          '1.25': '0.3125rem',      //5px          
          '2.5': '0.625rem',        //10px 
          '2.75': '0.6875rem',    	//11px 
          '3.75': '0.9375rem',     	//15px
          '5.25': '1.3125rem',    	//21px           
          '7.5': '1.875rem',        //30px       
          '12.5': '3.125rem',       //50px
          '13.5': '3.375rem',       //54px
          '15': '3.75rem',          //60px 
          '15.5': '3.875rem',       //62px           
          '17.5': '4.375rem',       //70px 
          '22.5': '5.625rem',       //90px 
          '25': '6.25rem',          //100px  
          '33.75': '8.4375rem',     //135px 
          '30': '7.5rem',           //120px
          '50': '12.5rem',          //200px 
          '78': '19.5rem',          //312px 
          '95': '23.75rem',         //380px 
          '200': '50rem',           //800px    

          
        },
     
        maxWidth: {
            
         }
      },


      fontFamily: {             
        'acumin': ['acumin-pro, sans-serif'],
        'lexia': ['lexia, serif'],
        'lexiaBig': ['lexia-advertising, serif'],
      },    



      
  },
  variants: {
    extend: {
      scale: ['active', 'group-hover'],
      textColor: ['group-hover'],
      borderRadius: ['hover', 'focus'],
      mixBlendMode: ['hover', 'focus'],
      translate: ['hover', 'group-hover'],
      opacity: ['hover', 'group-hover'],
      transform: ['hover', 'group-hover'],
      
    },
  },
  plugins: [],
}
}

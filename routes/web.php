<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\UpdateFormController;
use App\Http\Controllers\GetFormController;
use App\Http\Controllers\CreateUserController;
use App\Http\Controllers\LoginUserController;
use App\Http\Controllers\FormData;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::statamic('example', 'example-view', [
//    'title' => 'Example'
// ]);


//Route::statamic('{course_uri}/start', 'courses.courseStart');
Route::statamic('/courses/{course_uri}/{start}', 'courses.courseDetail');
Route::post('/forms/course', [UpdateFormController::class, 'index']);
Route::post('/forms/user/create', [CreateUserController::class, 'index']);
Route::post('/forms/login', [LoginUserController::class, 'index']);
Route::get('/forms/get-user-course', [GetFormController::class, 'index']);


Route::statamic('/housing-support-training/{course_uri}/results', 'results');
Route::statamic('/registered', 'registered');
Route::statamic('/reset-password', 'resetPassword');
Route::statamic('/forgot-password', 'forgotPassword');
Route::statamic('/register', 'register');
Route::statamic('/login', 'login');
Route::get('/results/formdata', [FormData::class, 'index']);


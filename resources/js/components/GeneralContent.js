import { useState, useEffect } from "react";

import Accordion from './Accordion';
import Audio from './Audio';
import Blockquote from './Blockquote';
import Questionnaire from "./Questionnaire";
import SpeechBubbles from './SpeechBubbles';
import ThreeColBlocks from "./ThreeColBlocks";
import Organisations from "./Organisations";
import TwoColBlocks from "./TwoColBlocks";
import FourColBlocks from "./FourColBlocks";
import Image from "./Image";
import Video from './Video';

let number = 0;

export default function GeneralContent(props) {
    
    const { items, captureQuestionnaire, submitQuestionnaire, disablenext, organisations, isFinished, currentStep } = props;
    useEffect(() => {

    })
    return (
    <div className="trainingWrapper text-primary w-full lg:w-8/12 pt-[20px] lg:pt-[40px] pb-[27.5vh] ">
        {items.map(function(row, index){
            const { type } = row;
            if(type === 'text') {
                const {text} = row;
                return <div key={'general-content-' + index} className="leading-thirty general-text" dangerouslySetInnerHTML={{ __html: text }} />
            } else if (type === 'questionnaire') {
                if(isFinished === 'Yes') {
                    <div>Hello World</div>
                } else {
                    const {questions} = row;
                    number++;
                    return <Questionnaire key={'general-content-' + index} number={number} questions={questions} captureQuestionnaire={captureQuestionnaire} submitQuestionnaire={submitQuestionnaire} disablenext={disablenext} currentStep={currentStep} />
                }
            } else if (type === 'audio') {
                const { audio, audio_description } = row;
                return <Audio key={'general-content-' + index} audio={audio} audio_description={audio_description} />
            } else if (type === '4_col_hover_blocks') {
                const { blocks_main_title, blocks } = row;
                return <FourColBlocks key={'general-content-' + index} blocks={blocks} blocks_main_title={blocks_main_title} />
            } else if (type === 'image') {
                const { image } = row;
                return <Image key={'general-content-' + index} image={image} />
            } else if (type === 'speech_bubbles') {
                const { speech_bubbles } = row;
                return <SpeechBubbles key={'general-content-' + index} speech_bubbles={speech_bubbles} />
            } else if (type === 'video') {
                const { video, video_url, video_description, video_placeholder_image, video_length, video_link } = row;
                if(video_link !== null) {
                    const embed = video_link.split('/');
                    const video_id = embed[embed.length - 1].replace('watch?v=', '');
                    return <Video key={'general-content-' + index} video_description={video_description} video_length={video_length} video_link={`https://www.youtube.com/embed/${video_id}`}  />
                } else if (video_url !== null){
                    if(video_placeholder_image === null) {
                        const video_img = "/assets/placeholders/banner6.png";
                        const video_link = video_url.url;
                        return <Video key={'general-content-' + index}  video_link={video_link} video_description={video_description} video_img={video_img}  video_length={video_length}  />
                    } else {
                        const video_link = video_url.url;
                        const video_img = video_placeholder_image.url;
                        return <Video key={'general-content-' + index}  video_link={video_link} video_description={video_description} video_img={video_img}  video_length={video_length}  />
                    }  
                }
            } else if (type === 'accordion') {
                const { title, content } = row;
                return <Accordion key={'general-content-' + index} title={title} content={content} />
            } else if (type === '3_col_hover_blocks') {
                const { blocks, blocks_main_title } = row;
                return <ThreeColBlocks key={'general-content-' + index} blocks={blocks} blocks_main_title={blocks_main_title} />
            } else if (type === 'blockquote') {
                const { content } = row;
                return <Blockquote key={'general-content-' + index} content={content} />
            } else if (type === 'organisations') {
                return <Organisations key={'general-content-' + index} organisations={organisations} />
            } else {
                return <p key={'general-content-' + index}>Hello world {type}</p>
            }
        })}
    </div>
  )
}
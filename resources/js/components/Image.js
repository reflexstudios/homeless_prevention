import { useState, useEffect } from "react";


export default function Image(props) {
    const { image } = props;
    return (
        <>
            {image.map(function(row, index){
                    const { url } = row;
                    return (
                        <div key={'image-' + index} className="bg-white py-[20px] md:py-[40px] px-[20px] md:px-[100px] mt-[40px] mb-[50px] rounded ">
                            <img className="object-contain h-full w-full" src={ url } alt="" />
                        </div>
                    )
            })}
        </>
    )
}
import React, { useState, useEffect } from "react";

export default function Questionnaire(props) {
    const { questions, captureQuestionnaire, submitQuestionnaire, number, disablenext, isLast, currentStep } = props;
    const [selected, setSelected] = useState([]);
    const completed = selected.filter(o => Object.values(o).some(v => v !== null));
    const formComplete = false;

    console.log("completed", selected);

    if (completed.length == questions.length) {
        submitQuestionnaire();
    } else {
        disablenext();
    }


    const onClick = (index, currentStep, question, option, isLast) => {
        let updated = selected;
        updated[index] = option;

        setSelected(updated);
        captureQuestionnaire(number, index, currentStep, question, option);

    }
    return (
        <div className="mt-[40px] flex flex-col gap-y-[20px]">
            {questions.map(function(row, index){
                const { question, option_choices, scale_text, label_left, label_right, display_text } = row;
                const isLast = (index + 1) === questions.length;
                const [selectedInput, setSelectedInput] = useState(-1);
                return (
                <div key={'question-' + index} className="p-[20px] md:p-[60px] bg-darkerPinkyWhite hover:bg-white transition rounded-xl">
                    <p className="font-bold ">
                        {display_text ? display_text : question} 
                        </p>
                    <p className="text-[11px]"> { scale_text }</p>
                    <div className="container">
                        <div className="questionnaire_radio_container flex-wrap">
                            {option_choices.map(function(choice, choiceIndex) {
                                const {option} = choice;
                                const active = (selectedInput === choiceIndex);
                                return (
                                    <React.Fragment key={'question-wrap-' + index + '-' + choiceIndex}>
                                        <input onChange={() => {}} className={`questionnaireRadio ${active ? "active" : ""}`} type="radio" name={`question[${number}][${index}]`} required/>
                                        <label onClick={() => { setSelectedInput(choiceIndex); onClick(index, currentStep, question, option, isLast)}} className="w-max p-[10px]  mb-5 ">{ option }</label>
                                    </React.Fragment>
                                )

                            })}
                        </div>

                        <div className=" w-full hidden md:flex justify-between mt-[10px] uppercase text-2xs">
                            <span>{ label_left }</span>
                            <span>{ label_right }</span>
                        </div>
                    </div>

                </div>
            )})}
        </div>
    )
}
import { useState, useEffect } from "react";


export default function FourColBlocks(props) {
    const { blocks, blocks_main_title} = props;
    return (
        <div className="mt-[40px]">
            <div className="flex flex-col md:flex-row items-center justify-between gap-y-[20px] gap-x-[20px]">
                <p className="text-normal text-center md:text-left md:text-1xl">{ blocks_main_title }</p>
                <div className="py-[8px] px-[14px] bg-lightGreen flex border-2 border-seaGreen rounded-lg max-w-max">
                    <svg className="mr-[10px]" width="20" height="19" viewBox="0 0 20 19" fill="none" xmlns="http://www.w3.org/2000/svg">
                        <path d="M10 18.75C9.96953 18.75 9.93859 18.7478 9.9075 18.7433C9.66828 18.7073 9.47078 18.5364 9.40125 18.3044L5.65125 5.80437C5.58531 5.58437 5.64547 5.34563 5.80813 5.18297C5.97109 5.02031 6.21125 4.95984 6.42953 5.02609L18.9295 8.77609C19.1614 8.84562 19.3323 9.04313 19.3684 9.28234C19.4042 9.52188 19.2983 9.76063 19.0969 9.89484L15.9814 11.9719L19.1922 15.183C19.4364 15.4272 19.4364 15.8227 19.1922 16.0667L16.6922 18.5667C16.448 18.8109 16.0525 18.8109 15.8084 18.5667L12.5973 15.3559L10.5203 18.4714C10.4031 18.6472 10.2069 18.75 10 18.75ZM12.5 13.75C12.6652 13.75 12.8241 13.8153 12.9419 13.9331L16.25 17.2413L17.8661 15.625L14.5581 12.3169C14.4256 12.1844 14.3597 11.9998 14.3781 11.8133C14.3964 11.6272 14.4975 11.4586 14.6534 11.3548L17.2981 9.59188L7.1825 6.55719L10.2172 16.6728L11.9802 14.0281C12.0839 13.8722 12.2523 13.7711 12.4386 13.7528C12.4591 13.7509 12.4795 13.75 12.5 13.75ZM4.19187 3.56687C4.43609 3.32266 4.43609 2.92719 4.19187 2.68313L2.94187 1.43313C2.69766 1.18891 2.30219 1.18891 2.05813 1.43313C1.81406 1.67734 1.81391 2.07281 2.05813 2.31687L3.30813 3.56687C3.43016 3.68891 3.59 3.75 3.75 3.75C3.91 3.75 4.06984 3.68891 4.19187 3.56687ZM3.75 5.625C3.75 5.27984 3.47016 5 3.125 5H1.25C0.904844 5 0.625 5.27984 0.625 5.625C0.625 5.97016 0.904844 6.25 1.25 6.25H3.125C3.47016 6.25 3.75 5.97016 3.75 5.625ZM2.94187 9.81688L4.19187 8.56687C4.43609 8.32266 4.43609 7.92719 4.19187 7.68313C3.94766 7.43906 3.55219 7.43891 3.30813 7.68313L2.05813 8.93313C1.81391 9.17734 1.81391 9.57281 2.05813 9.81688C2.18016 9.93891 2.34 10 2.5 10C2.66 10 2.81984 9.93891 2.94187 9.81688ZM9.19187 3.56687L10.4419 2.31687C10.6861 2.07266 10.6861 1.67719 10.4419 1.43313C10.1977 1.18906 9.80219 1.18891 9.55813 1.43313L8.30813 2.68313C8.06391 2.92734 8.06391 3.32281 8.30813 3.56687C8.43016 3.68891 8.59 3.75 8.75 3.75C8.91 3.75 9.06984 3.68891 9.19187 3.56687ZM6.875 2.5V0.625C6.875 0.279844 6.59516 0 6.25 0C5.90484 0 5.625 0.279844 5.625 0.625V2.5C5.625 2.84516 5.90484 3.125 6.25 3.125C6.59516 3.125 6.875 2.84516 6.875 2.5Z" fill="#015C71"/>
                    </svg>
                    <span className="text-2xs mt-0.5 flex min-w-max">
                    <span className="hidden md:flex">Hover</span>
                    <span className="flex md:hidden">Tap</span>
                     on the cards to reveal the answers</span>
                </div>
            </div>

            <div className="flex flex-wrap justify-center gap-x-[20px] gap-y-[20px] mt-[40px]">
                {blocks.map(function(row, index){
                    const { icon, text } = row;
                    return (
                        <div key={'four-col-blocks-' + index} className="bg-white px-[70px] min-w-[calc(50%-20px)] md:min-w-[calc(25%-20px)] max-w-[215px] min-h-[192px] py-[80px] rounded-lg relative flex items-center justify-center overflow-hidden group hover:bg-darkPurple transition cursor-default">
                            <div className="absolute flex items-center justify-center top-1/2 left-1/2 -translate-x-1/2 -translate-y-1/2 group-hover:translate-x-52 transition">
                                <img src={icon.url} alt="" />
                            </div>
                            <div className="absolute  flex items-center justify-center top-1/2 left-1/2 translate-x-52 -translate-y-1/2 group-hover:-translate-x-1/2 transition ">
                                <p className="text-center text-white">{ text }</p>
                            </div>
                        </div>
                    )
                })}
            </div>
        </div>
    )
}
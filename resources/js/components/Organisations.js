import { useState, useEffect } from "react";

export default function Organisations(props) {
    const {organisations} = props;
    



    
    return (
        <>
                {organisations.map(function(row, index){
                    const { title, logo, target_group, referral_source, replicator } = row;
                    const [isOpened, setIsOpened] = useState(false);
                    return (
                        <div className={`flex flex-col gap-y-[10px] mt-[10px] mb-[20px]`}>
                        <li className={`list-none rounded-lg hover:bg-white transition ${(isOpened === true) ? 'bg-white' : 'bg-darkerPinkyWhite'}`}>
                            <button onClick={() => setIsOpened(!isOpened)} type="button" className="w-full p-[40px] text-left group">
                            <div className="flex flex-col sm:flex-row items-center justify-start">
                            
                                <div class="p-[16px] min-h-[130px] min-w-[130px] h-[130px] w-[130px] sm:mr-[30px] rounded-full overflow-hidden transition flex justify-center items-center border-pinkyWhite bg-white">
                                
                                    <img src={logo.url} />
                                </div>
                            
                                <div className="flex  items-center justify-start flex-col sm:flex-row flex-grow">
                                    <div className="flex justify-start items-center flex-col">
                                        <p className="text-normal font-bold  lg:text-left">{ title }</p>
                                    </div>
                                </div>
            
                                <svg 
                                    className={`transition fill-current text-darkPurple mt-8 sm:mt-0 transform group-hover:scale-125 ${(isOpened === true) ? 'rotate-180' : 'rotate-0'}`}
                                    width="10"
                                    height="6"
                                    viewBox="0 0 10 6"
                                    fill="none"
                                    xmlns="http://www.w3.org/2000/svg"
                                >
                                    <path fillRule="evenodd" clipRule="evenodd"
                                        d="M9.78033 0.46967C10.0732 0.762563 10.0732 1.23744 9.78033 1.53033L5.78033 5.53033C5.48744 5.82322 5.01256 5.82322 4.71967 5.53033L0.71967 1.53033C0.426777 1.23744 0.426777 0.762563 0.71967 0.46967C1.01256 0.176777 1.48744 0.176777 1.78033 0.46967L5.25 3.93934L8.71967 0.46967C9.01256 0.176777 9.48744 0.176777 9.78033 0.46967Z"
                                        fill="#59235F" />
                                </svg>
            
                            </div>
                            </button>
            
                            <div className={`relative overflow-hidden transition-all duration-700 ${(isOpened === true) ? '' : 'max-h-0'}`}>
                                <div className="pb-[40px] px-[40px] w-full  text-darkPurple flex flex-col">
                                    <div className="leading-twentyeight flex flex-col">
                                    
                                    <div class="font-acumin uppercase font-bold text-xs mb-4">Target Group</div>
                                        {target_group}
                                  
                                    
                                    <div class="mt-10"></div>
                                    <div class="font-acumin uppercase font-bold text-xs mb-4">Referral Source</div>
                                        {referral_source}
                                    
                                    </div>
                                    <div class="mt-10"></div>
                                    <div class="font-acumin uppercase font-bold text-xs mb-4">Contact detail</div>
                                    <div class="flex flex-col md:flex-row flex-wrap justify-between gap-x-[20px] w-full gap-y-[20px] ">
                                        {replicator.map(function(row, index){
                                            const { phone_number, name, location, role, email } = row;
                                            return (
                                                <div class="flex flex-col flex-1 w-full md:w-1/2 contactBlockCard bg-pinkyWhite p-[30px]">
                                                
                                                    <div className={`w-full flex justify-end mb-[30px] ${(location === null) ? 'opacity-0' : ''}`}>
                                                        <p class="flex items-center">
                                                            <svg width="10" height="10" viewBox="0 0 7 8" fill="none" xmlns="http://www.w3.org/2000/svg">
                                                            <path d="M3.4076 0C1.70244 0 0.315186 1.38726 0.315186 3.09241C0.315186 3.95447 0.786544 4.99567 1.71617 6.1871C2.38013 7.03805 3.03636 7.6534 3.06395 7.67918L3.40756 8L3.75114 7.67918C3.77876 7.6534 4.435 7.03805 5.09895 6.1871C6.02861 4.99567 6.49997 3.95447 6.49997 3.09241C6.50004 1.38729 5.11278 0 3.4076 0ZM3.4076 4.09289C2.85595 4.09289 2.40715 3.64409 2.40715 3.09244C2.40715 2.5408 2.85595 2.09199 3.4076 2.09199C3.95928 2.09199 4.40808 2.5408 4.40808 3.09244C4.40808 3.64409 3.95928 4.09289 3.4076 4.09289Z" fill="#59235F"/>
                                                            </svg>
                                                            <span class="ml-[3px] uppercase font-bold text-xs">{ location }</span>
                                                        </p>
                                                    </div>
                                                    <div class="flex flex-col gap-y-[10px]">
                                                  
                                                    <p class="font-bold">{ name }</p>
                                                 
                                                    <p class="">{ role }</p>
                                                    <p class="break-all hover:text-seaGreen transition">{ email }</p>
                                                  
                                                    <p class="break-all hover:text-seaGreen transition">{ phone_number }</p>
                                                    
                                                    </div>
                                                </div>
                                            )
                                        })}
                                    </div>
                                </div>
                            </div>
                        </li>
                    </div>
                    )
                })}
        </>
  )
}
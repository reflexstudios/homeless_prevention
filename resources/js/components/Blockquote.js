import { useState, useEffect } from "react";


export default function Blockquote(props) {
    const {content} = props;
    return (
        <blockquote className="pt-[80px] md:pt-[40px] pl-[40px] pb-[40px] pr-[40px] md:pr-[112px] text-darkPurple bg-white relative my-[40px] rounded-xl">
            <div dangerouslySetInnerHTML={{ __html: content }} />
            <img src="/assets/icons/quotes.svg" alt="" className="absolute top-[20px] right-[20px] md:top-[40px] md:right-[40px]" />
        </blockquote>
    )
}
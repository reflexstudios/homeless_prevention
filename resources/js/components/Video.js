import { useState, useEffect, useRef } from "react";
import { Player, BigPlayButton } from 'video-react';

export default function Video(props) {
    const { video, video_url, video_description, video_length, video_link, video_img } = props;
    
    
    return (
        <div className="relative mt-[40px]">

            <div className="mb-[40px] gap-y-[20px] lg:gap-x-[20px] lg:gap-x-[40px] flex flex-col md:flex-row justify-between items-end ">
            <div>
                <span className="text-1xl">{ video_description }</span>
            </div>
            <div className="flex items-center w-max ">
                <svg width="14" height="15" viewBox="0 0 14 15" fill="none" xmlns="http://www.w3.org/2000/svg">
                <g clipPath="url(#clip0_238_1354)">
                    <path
                    d="M7 14.5C10.8593 14.5 14 11.3593 14 7.5C14 3.64067 10.8594 0.5 7 0.5C3.14063 0.5 0 3.64067 0 7.5C0 11.3593 3.14067 14.5 7 14.5ZM7 1.43332C10.346 1.43332 13.0667 4.15398 13.0667 7.5C13.0667 10.846 10.346 13.5667 7 13.5667C3.65398 13.5667 0.933318 10.846 0.933318 7.5C0.933318 4.15398 3.65401 1.43332 7 1.43332Z"
                    fill="#59235F" />
                    <path
                    d="M9.04165 9.73071C9.128 9.8007 9.23063 9.83339 9.33331 9.83339C9.47098 9.83339 9.60631 9.77272 9.69729 9.65839C9.8583 9.45772 9.82561 9.16372 9.62497 9.00271L7.46664 7.27604V3.76671C7.46664 3.51004 7.25665 3.30005 6.99998 3.30005C6.74331 3.30005 6.53333 3.51004 6.53333 3.76671V7.50006C6.53333 7.6424 6.59867 7.77539 6.70832 7.86404L9.04165 9.73071Z"
                    fill="#59235F" />
                </g>
                <defs>
                    <clipPath id="clip0_238_1354">
                        <rect width="14" height="14" fill="white" transform="translate(0 0.5)" />
                    </clipPath>
                </defs>
                </svg>
                <small className="ml-2 w-max">{ video_length }</small>
            </div>
            </div>
            
            
            <Player
                playsInline
                poster={ video_img }
                src={ video_link }>
                <BigPlayButton position="center" />
            </Player>
     
        </div>
    )
}
import { useState, useEffect } from "react";

export default function HeroBanner(props) {
    const { hero_banner, title, title_line_1, title_line_2 } = props;
    useEffect(() => {

    })
    var divStyle = {
        backgroundImage: 'url(' + hero_banner + ')',
        backgroundSize: 'cover',
        backgroundRepeat: 'no-repeat',
        backgroundPosition: 'center'
      };
    return (
        <>
            <div style={divStyle} className="h-[200px] lg:h-[360px] w-full lg:w-9/12 relative"></div>
            <div className="trainingWrapper w-full md:pb-[40px] pt-[37px] lg:pt-[18px] relative mt-[5px] lg:mt-[3px] sm:mb-0">
                <div className="sm:absolute mt-[-20vw] sm:mt-auto transform sm:-translate-y-24 lg:-translate-y-24 left-10 lg:left-20">
                    <p className="bg-darkPurple max-w-[80vw] md:max-w-auto text-white text-[1.75rem] lg:text-[2.5vw]  2xl:text-fifty font-lexia font-bold uppercase w-max px-[10px]">
                    { title_line_1 }
                    </p>
                    <p className="bg-darkPurple max-w-[80vw] md:max-w-auto break1:max-w-[100%] text-white text-[1.75rem] lg:text-[2.5vw]  2xl:text-fifty font-lexia font-bold uppercase w-max px-[10px]">
                    { title_line_2 }
                    </p>
                </div>
            </div>
        </>
  )
}
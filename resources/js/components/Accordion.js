import { useState, useEffect } from "react";


export default function Accordion(props) {
    const {content, title} = props;
    const [isOpened, setIsOpened] = useState(false);
    return (
        <div className={`flex flex-col gap-y-[10px] mt-[10px]`}>
            <li className={`list-none rounded-lg hover:bg-white transition ${(isOpened === true) ? 'bg-white' : 'bg-darkerPinkyWhite'}`}>
                <button onClick={() => setIsOpened(!isOpened)} type="button" className="w-full p-[40px] text-left group">
                <div className="flex flex-col sm:flex-row items-center justify-between">
                    <div className="flex  items-center justify-center flex-col sm:flex-row">
                        <div className="flex justify-center items-center flex-col">
                            <p className="text-normal font-bold text-center lg:text-left">{ title }</p>
                        </div>
                    </div>

                    <svg 
                        className={`transition fill-current text-darkPurple mt-8 sm:mt-0 transform group-hover:scale-125 ${(isOpened === true) ? 'rotate-180' : 'rotate-0'}`}
                        width="10"
                        height="6"
                        viewBox="0 0 10 6"
                        fill="none"
                        xmlns="http://www.w3.org/2000/svg"
                    >
                        <path fillRule="evenodd" clipRule="evenodd"
                            d="M9.78033 0.46967C10.0732 0.762563 10.0732 1.23744 9.78033 1.53033L5.78033 5.53033C5.48744 5.82322 5.01256 5.82322 4.71967 5.53033L0.71967 1.53033C0.426777 1.23744 0.426777 0.762563 0.71967 0.46967C1.01256 0.176777 1.48744 0.176777 1.78033 0.46967L5.25 3.93934L8.71967 0.46967C9.01256 0.176777 9.48744 0.176777 9.78033 0.46967Z"
                            fill="#59235F" />
                    </svg>

                </div>
                </button>

                <div className={`relative overflow-hidden transition-all duration-700 ${(isOpened === true) ? 'max-h-96' : 'max-h-0'}`}>
                    <div className="pb-[40px] px-[40px] w-full md:w-9/12  text-darkPurple flex flex-col">
                        <div className="leading-twentyeight flex flex-col">
                        {content.map(function(row, index){
                            const { type } = row;
                            if(type === 'text') {
                                const {text} = row;
                                return <div key={'general-accordian-content-' + index} className="flex flex-1 w-full flex-col" dangerouslySetInnerHTML={{ __html: text }} />
                            }
                        })}
                        </div>
                    </div>
                </div>
            </li>
        </div>
    )
}
import { useState, useEffect } from "react";

export default function SidebarFooter(props) {
    const { prevStep, nextStep, disableButtons, progress, total, complete, mobileMenu, menuOpen, isFinished } = props;
   
    return (
    <div className="fixed z-50 bottom-0 w-full lg:w-4/12 bg-white px-[20px] py-[20px] flex flex-col-reverse lg:flex-col -ml-8 transform translate-x-[2px] lg:gap-y-[20px]">
        <div className="justify-between hidden lg:flex">
            <span className="font-acumin uppercase text-2xs">Your Progress</span>
            <span className="font-acumin text-2xs">{progress}%</span>
        </div>

        <div className="flex items-center justify-between gap-x-[10px] mt-[20px] lg:mt-0">
            <div className="h-[14px] w-full bg-pinkyWhite rounded-full relative">
                <div className="transition-all h-[14px] bg-darkPurple rounded-full absolute" style={{"width" : progress + "%"}}></div>
            </div>
            <span className="font-acumin text-normal flex lg:hidden">{progress}%</span>
        </div>
        <div 
                    className={`w-full bg-pinkyWhite text-xs mt-6 lg:mt-0 font-bold text-black p-4 rounded ${(disableButtons === true) ? 'block' : 'hidden'}`}
                    >
                        Please complete the questionnaire to continue
                    </div>
        <div className="flex items-center gap-x-[10px]">
            <button id="footerNavBtn" onClick={mobileMenu} className="flex lg:hidden  transition bg-pinkyWhite rounded-full w-[50px] h-[50px] group">
                <div className={`openIcon flex-col justify-center items-center gap-y-[6px] rounded-full w-[50px] h-[50px] ${(menuOpen === true) ? 'hidden' : 'flex'}`}>
                    <div className="w-[20px] h-[2px] bg-darkPurple transition rounded-full"></div>
                    <div className="w-[10px] h-[2px] bg-darkPurple transition rounded-full"></div>
                    <div className="w-[20px] h-[2px] bg-darkPurple transition rounded-full"></div>
                </div>
                <div className={`closeIcon bg-darkPurple flex-col justify-center items-center gap-y-[12px] rounded-full w-[50px] h-[50px] ${(menuOpen === true) ? 'flex' : 'hidden '}`}>
                    <svg width="30" height="30" viewBox="0 0 30 30" fill="none" xmlns="http://www.w3.org/2000/svg">
                        <rect width="20" height="2" rx="1" transform="matrix(0.707107 -0.707107 -0.707107 -0.707107 8.63604 22.7782)" fill="white"/>
                        <rect width="2" height="20" rx="1" transform="matrix(0.707107 -0.707107 -0.707107 -0.707107 21.364 22.7782)" fill="white"/>
                    </svg>            
                </div>
            </button>

            {(progress === 100) 
                ?
                <>
                    {(isFinished === 'Yes' ) ?
                        <>
     
     <button onClick={prevStep}
                    className={`md:px-[30px] min-w-max md:w-1/2 flex-1 py-[18px] font-semibold bg-darkPurple hover:bg-seaGreen group rounded-full text-1xs transition ${(disableButtons === true) ? 'opacity-50 pointer-events-none' : 'show'}`}
                    >
                        <span className="text-white transition">Previous</span>
                    </button>
                    <button onClick={nextStep}
                    className={`md:px-[30px] min-w-max md:w-1/2 flex-1 py-[18px] font-semibold bg-seaGreen  hover:bg-darkPurple group rounded-full text-1xs transition ${(disableButtons === true) ? 'opacity-50 pointer-events-none' : 'show'}`} 
                    >
                        <span className="text-white">Next</span>
                    </button>
                  

                        </>
                            :
                        <>  
<button onClick={complete} className="md:px-[30px] min-w-max flex-1 py-[18px] font-semibold bg-darkPurple hover:bg-seaGreen group rounded-full text-1xs transition">
                        <span className="text-white transition">Finish</span>
                    </button>
                        
                        </>
                        }

                    
                </>
                :
                <>

                    <button onClick={prevStep}
                    className={`md:px-[30px] min-w-max md:w-1/2 flex-1 py-[18px] font-semibold bg-darkPurple hover:bg-seaGreen group rounded-full text-1xs transition ${(disableButtons === true) ? 'opacity-50 pointer-events-none' : 'show'}`}
                    >
                        <span className="text-white transition">Previous</span>
                    </button>
                    <button onClick={nextStep}
                    className={`md:px-[30px] min-w-max md:w-1/2 flex-1 py-[18px] font-semibold bg-seaGreen  hover:bg-darkPurple group rounded-full text-1xs transition ${(disableButtons === true) ? 'opacity-50 pointer-events-none' : 'show'}`} 
                    >
                        <span className="text-white">Next</span>
                    </button>
                </>
            }




        </div>
    </div>
    )
}
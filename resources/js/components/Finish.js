import { useState, useEffect } from "react";

export default function Finish(props) {
    const {user, login, signUp, saveCert, onLogin, isRegError, isLoggedInError, isCertificateSent, isLoaded, title } = props;

    const [firstName, setFirstName] = useState('');
    const [surname, setSurname] = useState('');
    const [sector, setSector] = useState('');
    const [email, setEmail] = useState('');
    const [confirmEmail, setConfirmEmail] = useState('');
    const [password, setPassword] = useState('');
    const [confirmPassword, setConfirmPassword] = useState('');
    const [maskPassword, setMaskPassword] = useState(true);
    const [showLogin, setShowLogin] = useState(false);

    const [firstNameError, setFirstNameError] = useState(false);
    const [surnameError, setSurnameError] = useState(false);
    const [sectorError, setSectorError] = useState(false);
    const [emailError, setEmailError] = useState(false);
    const [confirmEmailError, setConfirmEmailError] = useState(false);
    const [passwordError, setPasswordError] = useState(false);
    const [confirmPasswordError, setConfirmPasswordError] = useState(false);

    const [loginEmail, setLoginEmail] = useState('');
    const [loginPassword, setLoginPassword] = useState('');

    

        
        

    

    const onRegister = () => {
        console.log("clicked1 ");
        setFirstNameError((firstName === ''));
        setSurnameError((surname === ''));
        setSectorError((sector === ''));
        setEmailError((email === ''));
        setConfirmEmailError((confirmEmail === ''));
        setPasswordError((password === ''));
        setConfirmPasswordError((confirmPassword === ''));

        // is email valid
        if(/^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/i.test(email) === false) {
            setEmailError(true);
        }
        if(/^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/i.test(confirmEmail) === false) {
            setConfirmEmailError(true);
        }

        // does password and confirm password match
        if(password !== confirmPassword) {
            setPasswordError(true);
            setConfirmPasswordError(true);
        } else if (password === confirmPassword) {
            setPasswordError(false);
            setConfirmPasswordError(false);
        }

        console.log("firstNameError ", firstNameError);
        console.log("surnameError ", surnameError);
        console.log("sectorError ", sectorError);
        console.log("emailError ", emailError);
        console.log("confirmEmailError ", confirmEmailError);
        console.log("passwordError ", passwordError);
        console.log("confirmPasswordError ", confirmPasswordError);

        if(
            (firstNameError === false) && (surnameError === false) && (sectorError === false) &&
            (emailError === false) && (confirmEmailError === false) &&
            (passwordError === false) && (confirmPasswordError === false)
        ) {
            console.log("sending to signup ");
            signUp({
                firstName, surname, sector, email, password, confirmPassword
            })
        }
        console.log("clicked6 ");
    }

    useEffect(() => {
        
    })
    return (
        <div className="w-full h-screen relative">
            <div id="imageWrapper" className="w-full fixed flex md:h-sceen bg-anotherPurple top-0 left-0">
                <div id="image" style={{backgroundImage: "url('/assets/placeholders/background1.png')"}} className="flex fixed z-20 flex-1 h-screen w-screen mix-blend-multiply grayscale opacity-[.67] bg-cover bg-center"></div>
                <div id="bgGradient" className="w-full bg-gradient z-10 flex h-sceen absolute top-0 left-0 h-screen"></div>
            </div>
        
            <div id="content"
                className={`absolute z-40 w-screen py-[50px] md:py-0 h-screen wrapper flex gap-y-[40px] md:justify-center items-center flex-col md:flex-row  my-10`}>

                <div className="flex md:justify-center items-center">
                    <div className={`bg-white py-[60px] rounded-lg flex sm:m-10 flex-col md:justify-center items-center `}>
                    
                    {(user.name) && 
                        <>
                            <div id="loggedIn" className="flex flex-col justify-center items-center  px-[20px] md:mx-[240px]">
                            <div className="flex flex-col items-center justify-center">
                                <svg width="60" height="71" viewBox="0 0 60 71" fill="none" xmlns="http://www.w3.org/2000/svg">
                                <circle cx="29.7227" cy="36" r="18" fill="#DBECF0" />
                                <path
                                    d="M32.8284 14.4432H13.5997C12.7349 14.4432 12.0336 13.742 12.0336 12.8771C12.0336 12.0122 12.7349 11.311 13.5997 11.311H32.8284C33.6934 11.311 34.3945 12.0122 34.3945 12.8771C34.3945 13.742 33.6933 14.4432 32.8284 14.4432Z"
                                    fill="#59235F" />
                                <path
                                    d="M32.8284 23.492H13.5997C12.7349 23.492 12.0336 22.7907 12.0336 21.9259C12.0336 21.061 12.7349 20.3598 13.5997 20.3598H32.8284C33.6934 20.3598 34.3945 21.061 34.3945 21.9259C34.3945 22.7907 33.6933 23.492 32.8284 23.492Z"
                                    fill="#59235F" />
                                <path
                                    d="M24.9107 32.5408H13.5998C12.7349 32.5408 12.0336 31.8395 12.0336 30.9747C12.0336 30.1098 12.7349 29.4086 13.5998 29.4086H24.9107C25.7756 29.4086 26.4769 30.1098 26.4769 30.9747C26.4769 31.8395 25.7756 32.5408 24.9107 32.5408Z"
                                    fill="#59235F" />
                                <path
                                    d="M39.8774 45.9923C39.1576 45.5125 38.9632 44.5401 39.443 43.8206C39.9228 43.1009 40.8952 42.9064 41.6148 43.3861L43.7051 44.7796L47.3609 39.2961C47.8405 38.5765 48.8131 38.382 49.5326 38.8617C50.2524 39.3416 50.4468 40.314 49.9671 41.0335L45.4428 47.82C45.141 48.2722 44.6453 48.5178 44.1373 48.5178C43.9465 48.5178 43.8507 48.5062 43.7521 48.4765C43.6418 48.4434 43.4886 48.3754 43.2094 48.2081C42.8475 47.9912 42.3555 47.6592 41.5807 47.1362L41.5784 47.1347C41.1205 46.8257 40.564 46.4501 39.8774 45.9923Z"
                                    fill="#59235F" />
                                <path fillRule="evenodd" clipRule="evenodd"
                                    d="M49.0988 4.21641V29.9317L49.5802 30.0878C59.602 33.3364 62.6372 46.1475 54.9681 53.5152L54.7543 53.7206V69.432C54.7543 70.2958 54.0528 70.9981 53.1882 70.9981L53.1504 70.9981C52.961 70.9982 52.9383 70.9982 52.8633 70.9762C52.7483 70.9424 52.5458 70.8555 52.0735 70.5998C51.5224 70.3015 50.6814 69.8176 49.3065 69.0264C48.3575 68.4802 47.1539 67.7877 45.6159 66.9088L45.2705 66.7114L38.1298 70.7918C37.085 71.3887 35.7867 70.6302 35.7867 69.432V60.8182H4.93907C2.61467 60.8182 0.722656 58.9262 0.722656 56.6018V4.21641C0.722656 1.89201 2.61467 0 4.93907 0H44.8823C47.2067 0 49.0988 1.89204 49.0988 4.21641ZM45.2705 29.4086H45.9666V4.21641C45.9666 3.61792 45.4809 3.13221 44.8824 3.13221H4.93907C4.34056 3.13221 3.85487 3.61791 3.85487 4.21641V56.6018C3.85487 57.2003 4.34056 57.686 4.93907 57.686H35.7867V53.7206L35.5729 53.5152C32.9163 50.963 31.2623 47.3797 31.2623 43.4168C31.2623 35.6932 37.5469 29.4086 45.2705 29.4086ZM46.0475 63.5479L51.6221 66.7333L51.6219 55.9436L50.6573 56.3471C47.2172 57.7859 43.3189 57.784 39.8835 56.3471L38.9188 55.9436V66.7333L44.4934 63.5479C44.975 63.2727 45.5659 63.2727 46.0475 63.5479ZM34.3945 43.4168C34.3945 49.4144 39.2728 54.2927 45.2705 54.2927C51.2682 54.2927 56.1465 49.4144 56.1465 43.4168C56.1465 37.4191 51.2682 32.5408 45.2705 32.5408C39.2728 32.5408 34.3945 37.4191 34.3945 43.4168Z"
                                    fill="#59235F" />
                                </svg>
                                <span className="text-3xl md:text-sixty text-center font-lexia font-bold uppercase">certificate</span>
                                <span className="font-acuim text-2.4xl uppercase font-semibold tracking-[.2em]">of completion</span>
                            </div>

                            <div
                                className="bg-pinkyWhite p-[20px] rounded-lg mt-[16px] flex flex-col items-center justify-center gap-y-[10px] md:w-[456px]">
                                <span>This certifcate is awarded to</span>
                                <span className="text-twentysix font-bold">{user.name}</span>
                            </div>

                            <div className="mt-[40px] flex flex-col items-center justify-center">
                                <p className="text-center mb-2">For successful completion of the {title}</p>
                                <strong className="text-center">Brought to you by the Homelessness Prevention Forum</strong>
                            </div>

                            <div className="flex flex-col md:flex-row gap-x-[10px] gap-y-[10px] mt-[40px]">
                                <a href="/" className="px-[30px] min-w-max py-[18px] font-semibold bg-darkPurple  group rounded-full text-1xs transition hover:bg-seaGreen ">
                                    <span className="text-white transition">Continue to Website</span>
                                </a>
                                <button disabled={isCertificateSent} onClick={saveCert} className={`px-[30px] min-w-[200px] py-[18px] font-semibold group rounded-full text-1xs transition ${(isCertificateSent === "true") ? 'bg-gray-400' : 'bg-seaGreen hover:bg-darkPurple'}`}>
                                {(isCertificateSent === "loading") ? 
                                <div className="overflow h-[15px w-full flex justify-center items-center">
                                    <img src="/assets/icons/loading.svg" alt="" className="w-[60px] h-[20px]" />
                                </div>
                                :
                                <span  className="text-white transition">{(isCertificateSent === "true") ? 'Certificate Sent' : 'Email Certificate'}</span>
                                }

                                

                                
                                </button>
                            </div>
                            </div>
                        </>
                    }

                    {(!user.name) && 
                    <>
                      <div id="loggedOut" className="flex flex-col items-center justify-center px-[20px] md:mx-[150px]">
                        <div className="flex flex-col items-center justify-center">
                            <svg width="60" height="71" viewBox="0 0 60 71" fill="none" xmlns="http://www.w3.org/2000/svg">
                                <circle cx="29.7227" cy="36" r="18" fill="#DBECF0" />
                                <path
                                d="M32.8284 14.4432H13.5997C12.7349 14.4432 12.0336 13.742 12.0336 12.8771C12.0336 12.0122 12.7349 11.311 13.5997 11.311H32.8284C33.6934 11.311 34.3945 12.0122 34.3945 12.8771C34.3945 13.742 33.6933 14.4432 32.8284 14.4432Z"
                                fill="#59235F" />
                                <path
                                d="M32.8284 23.492H13.5997C12.7349 23.492 12.0336 22.7907 12.0336 21.9259C12.0336 21.061 12.7349 20.3598 13.5997 20.3598H32.8284C33.6934 20.3598 34.3945 21.061 34.3945 21.9259C34.3945 22.7907 33.6933 23.492 32.8284 23.492Z"
                                fill="#59235F" />
                                <path
                                d="M24.9107 32.5408H13.5998C12.7349 32.5408 12.0336 31.8395 12.0336 30.9747C12.0336 30.1098 12.7349 29.4086 13.5998 29.4086H24.9107C25.7756 29.4086 26.4769 30.1098 26.4769 30.9747C26.4769 31.8395 25.7756 32.5408 24.9107 32.5408Z"
                                fill="#59235F" />
                                <path
                                d="M39.8774 45.9923C39.1576 45.5125 38.9632 44.5401 39.443 43.8206C39.9228 43.1009 40.8952 42.9064 41.6148 43.3861L43.7051 44.7796L47.3609 39.2961C47.8405 38.5765 48.8131 38.382 49.5326 38.8617C50.2524 39.3416 50.4468 40.314 49.9671 41.0335L45.4428 47.82C45.141 48.2722 44.6453 48.5178 44.1373 48.5178C43.9465 48.5178 43.8507 48.5062 43.7521 48.4765C43.6418 48.4434 43.4886 48.3754 43.2094 48.2081C42.8475 47.9912 42.3555 47.6592 41.5807 47.1362L41.5784 47.1347C41.1205 46.8257 40.564 46.4501 39.8774 45.9923Z"
                                fill="#59235F" />
                                <path fillRule="evenodd" clipRule="evenodd"
                                d="M49.0988 4.21641V29.9317L49.5802 30.0878C59.602 33.3364 62.6372 46.1475 54.9681 53.5152L54.7543 53.7206V69.432C54.7543 70.2958 54.0528 70.9981 53.1882 70.9981L53.1504 70.9981C52.961 70.9982 52.9383 70.9982 52.8633 70.9762C52.7483 70.9424 52.5458 70.8555 52.0735 70.5998C51.5224 70.3015 50.6814 69.8176 49.3065 69.0264C48.3575 68.4802 47.1539 67.7877 45.6159 66.9088L45.2705 66.7114L38.1298 70.7918C37.085 71.3887 35.7867 70.6302 35.7867 69.432V60.8182H4.93907C2.61467 60.8182 0.722656 58.9262 0.722656 56.6018V4.21641C0.722656 1.89201 2.61467 0 4.93907 0H44.8823C47.2067 0 49.0988 1.89204 49.0988 4.21641ZM45.2705 29.4086H45.9666V4.21641C45.9666 3.61792 45.4809 3.13221 44.8824 3.13221H4.93907C4.34056 3.13221 3.85487 3.61791 3.85487 4.21641V56.6018C3.85487 57.2003 4.34056 57.686 4.93907 57.686H35.7867V53.7206L35.5729 53.5152C32.9163 50.963 31.2623 47.3797 31.2623 43.4168C31.2623 35.6932 37.5469 29.4086 45.2705 29.4086ZM46.0475 63.5479L51.6221 66.7333L51.6219 55.9436L50.6573 56.3471C47.2172 57.7859 43.3189 57.784 39.8835 56.3471L38.9188 55.9436V66.7333L44.4934 63.5479C44.975 63.2727 45.5659 63.2727 46.0475 63.5479ZM34.3945 43.4168C34.3945 49.4144 39.2728 54.2927 45.2705 54.2927C51.2682 54.2927 56.1465 49.4144 56.1465 43.4168C56.1465 37.4191 51.2682 32.5408 45.2705 32.5408C39.2728 32.5408 34.3945 37.4191 34.3945 43.4168Z"
                                fill="#59235F" />
                            </svg>
                            <span className="text-3xl md:text-sixty font-lexia font-bold uppercase">Thank you</span>
                        </div>

                        <div className="mt-[20px] flex flex-col items-center justify-center">
                            <p className="text-center mb-2 text-center">For successful completion of the Homelessness Prevention Training Course</p>
                            <strong className="text-center">Brought to you by the Homelessness Prevention Forum</strong>
                        </div>

                        <div className="mt-[40px] certReg flex flex-col items-center justify-center w-full">
                            <p className="text-center mb-2">Create an account to get your personalised certificate of completion</p>
                        </div>

                        {(isRegError === true) && 
                            <div className="mt-[40px] certReg flex flex-col items-center justify-center w-full">
                                <p className="text-center mb-2">Register error.  Please check your details and try again.</p>
                            </div>
                        }

                        
                        <div className="flex flex-col justify-between gap-x-[20px] gap-y-[20px] text-darkPurple mt-[60px] w-full md:w-[600px]">

                            <div className="flex flex-col w-full md:flex-row gap-x-[20px] gap-y-[20px]">
                                <input
                                    className={`bg-pinkyWhite flex w-full p-[20px] border-red-500 ${(firstNameError) ? 'border-b-3' : 'border-b-0'}`}
                                    name="first_name"
                                    type="text"
                                    placeholder="First Name"
                                    required
                                    value={firstName}
                                    onChange={(e) => { setFirstName(e.target.value); setFirstNameError(false)}}
                                />
                                <input 
                                    className={`bg-pinkyWhite flex w-full p-[20px] border-red-500 ${(surnameError) ? 'border-b-3' : 'border-b-0'}`}
                                    name="surname"
                                    type="text"
                                    placeholder="Surname"
                                    required
                                    value={surname}
                                    onChange={(e) => {setSurname(e.target.value); setSurnameError(false)}}
                                />
                            </div>

                            <div className="flex flex-col md:flex-row gap-x-[20px] gap-y-[20px]">
                                <input
                                    className={`bg-pinkyWhite flex w-full p-[20px] border-red-500 ${(emailError) ? 'border-b-3' : 'border-b-0'}`}
                                    name="email"
                                    type="text"
                                    placeholder="Email"
                                    value={email} 
                                    onChange={(e) => { setEmail(e.target.value); setEmailError(false)}} 
                                    required 
                                />
                                <input 
                                    className={`bg-pinkyWhite flex w-full p-[20px] border-red-500 ${(confirmEmailError) ? 'border-b-3' : 'border-b-0'}`}
                                    type="text"
                                    placeholder="Re-enter Email"
                                    value={confirmEmail}
                                    onChange={(e) => { setConfirmEmail(e.target.value); setConfirmEmailError(false)}} 
                                    required
                                />
                            </div>

                            <div className="flex flex-col md:flex-row gap-x-[20px] gap-y-[20px]">

                            <select 
                            name="sector" 
                            placeholder="sector"
                            required
                            value={sector}
                            onChange={(e) => {setSector(e.target.value); setSectorError(false)}}
                            className={`bg-pinkyWhite rounded flex w-full p-[20px] ${(sectorError) ? 'border-b-3' : 'border-b-0'}`} required>
                                <option value="">Sector:</option>
                                <option value="Advice sector">Advice sector</option>
                                <option value="General Public">General Public</option>
                                <option value="Health">Health</option>
                                <option value="Housing Association">Housing Association</option>
                                <option value="Housing Executive">Housing Executive</option>
                                <option value="Justice">Justice</option>
                                <option value="Other Non stat">Other Non stat</option>
                                <option value="Other stat">Other stat</option>
                                <option value="Private Landlord">Private Landlord</option>
                                <option value="Social care">Social care</option>
                                <option value="Other">Other</option>
                            </select>
                            </div>

                            <input type="text" className="hidden" name="" />

                            <div className="flex flex-col md:flex-row gap-x-[20px] gap-y-[20px] w-full">
                                <div className="relative w-full">
                                <input
                                    id="password"
                                    className={`bg-pinkyWhite flex w-full p-[20px] border-red-500 ${(passwordError) ? 'border-b-3' : 'border-b-0'}`}
                                    name="password" 
                                    type={(maskPassword === true) ? 'password' : 'text'}
                                    placeholder="Create Password"
                                    required value={password}
                                    onChange={(e) => { setPassword(e.target.value); setPasswordError()}}
                                />
                                <i id="togglePassword"
                                    onClick={() => setMaskPassword(!maskPassword)}
                                    className={`fas absolute top-1/2 transform -translate-y-1/2 right-4 cursor-pointer ${(maskPassword === true) ? 'fa-eye-slash' : 'fa-eye'}`}></i>
                                </div>
                                <div className="relative w-full">
                                <input
                                    id="confirmPassword"
                                    className={`bg-pinkyWhite flex w-full p-[20px] border-red-500 ${(confirmPasswordError) ? 'border-b-3' : 'border-b-0'}`}
                                    name="password_confirmation"
                                    type={(maskPassword === true) ? 'password' : 'text'}
                                    placeholder="Re-enter Password"
                                    value={confirmPassword}
                                    onChange={(e) => { setConfirmPassword(e.target.value); setConfirmPasswordError()}} 
                                    required 
                                />
                                <i id="toggleConfirmPassword"
                                    onClick={() => setMaskPassword(!maskPassword)}
                                    className={`fas absolute top-1/2 transform -translate-y-1/2 right-4 cursor-pointer ${(maskPassword === true) ? 'fa-eye-slash' : 'fa-eye'}`}></i>
                                </div>
                            </div>

                            </div>
                            <div className="flex justify-center items-center mt-[20px]">
                                <button
                                    onClick={onRegister}
                                    className="px-[30px] min-w-max py-[18px] font-semibold bg-seaGreen  group rounded-full text-1xs transition hover:bg-darkPurple text-white">Get
                                    Personalised Certificate
                                </button>
                            </div>
                        </div>

                        <div className="w-full flex  justify-center items-end mt-[20px] gap-x-[10px]">
                            <small>Already registered?</small>
                            <button onClick={() => setShowLogin(!showLogin)} className="font-semibold  text-1xs transition hover:text-seaGreen text-darkPurple">Log In</button>
                        </div>
                        <div className={`overflow-hidden transition-all duration-700 ${(showLogin === true) ? 'max-h-96' : 'max-h-0'}`}>

                            {(isLoggedInError === true) && <p>Check your username and password and try again</p>}

                        <div className={`flex flex-col md:flex-row gap-x-[20px] gap-y-[20px] pt-5`}>
                                <input className="bg-pinkyWhite flex w-full p-[20px]" name="email" type="text" placeholder="Email"
                                value={loginEmail} onChange={(e) => setLoginEmail(e.target.value)} required />
                                <input className="bg-pinkyWhite flex w-full p-[20px]" type={(maskPassword === true) ? 'password' : 'text'}  placeholder="Password"
                                value={loginPassword} onChange={(e) => setLoginPassword(e.target.value)} required />
                            </div>
                            <div onClick={() => onLogin(loginEmail, loginPassword)} className="flex justify-center items-center mt-[20px]">
                                <button
                                    
                                    className="px-[30px] min-w-max py-[18px] font-semibold bg-seaGreen  group rounded-full text-1xs transition hover:bg-darkPurple text-white">Login
                                </button>
                            </div>
                        </div>
                    </>}

                    
                    </div>
                </div>
            </div>
        </div>
  )
}


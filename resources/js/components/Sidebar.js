import { useState, useEffect } from "react";
import SidebarSection from './SidebarSection';
import SidebarFooter from './SidebarFooter';
import CompletedModal from './CompletedModal';

export default function Sidebar(props) {
    const { title, sections, prevStep, nextStep, goToStep, totalSteps, currentStep, step, disableButtons, complete, activeSidebarIndex, setActiveSidebarIndex, isFinished } = props;
    const [completedProgress, setCompletedProgress] = useState(0);
    const [openSidebar, setOpenSidebar] = useState(-1);

    const [ menuOpen, setMenuOpen ] = useState(false);

    function mobileMenu(){
        setMenuOpen(!menuOpen);
    }

    useEffect(() => {
        let totalComplete = 0;
        let totalMinutes = 0;
        
        sections.map((row) => {
            const isComplete = row.steps.filter((item) => item.isCompleted === true);
            row.steps.filter((item) => totalMinutes += item.lesson_length);
            totalComplete += isComplete.length;
        });

        

        let tmp = Math.round(((100 / totalSteps) * totalComplete));
        

        setCompletedProgress(tmp);
        setOpenSidebar(activeSidebarIndex);
        console.log("tmp", tmp);
        console.log("completedProgress", completedProgress);        
    })
    return (
        <div className="fixed z-40 right-0 h-0 w-full lg:w-4/12 lg:bg-darkPurple px-[30px] pb-[50px] ">
            <div className={`trainingNav fixed z-40 right-0 h-screen w-full lg:w-4/12 bg-darkPurple px-[30px] pb-[50px] ${(menuOpen === true) ? 'block lg:block' : 'hidden lg:block '}`}>
            
                <div className="fixed">
                    <div className="mt-[40px] lg:mt-[80px] w-10/12">
                        <p className="text-twentyEight uppercase font-bold text-white font-lexia leading-thirtyfive">{title}</p>
                    </div>
                    <div className="flex items-center mt-[20px] opacity-50">
                        <div className="rounded-full border-2 bg-pinkyWhite w-[40px] h-[40px] p-[6px] mr-2 ">
                            <img src="/assets/logos/miniLogo.png" alt="" className="object-contain w-full h-full" />
                        </div>
                        <span className="text-2xs text-white">By Homelessness Prevention Forum</span>
                    </div>
                </div>
                <div className="flex flex-col gap-y-[10px] mt-[200px] lg:mt-[240px] pt-[40px] pb-[440px] h-screen overflow-y-auto">
                    {sections.map((row, i) => 
                        <SidebarSection
                            key={'sidebar-section-' + i}
                            title={sections[i].title}
                            section={i}
                            step={step}
                            steps={sections[i].steps}
                            currentSidebar={activeSidebarIndex === i}
                            goToStep={goToStep}
                            isOpened={openSidebar === i}
                            onOpen={() => setOpenSidebar((openSidebar === i) ? -1 : i)}
                        />
                    )}
                </div>
            </div>
            
            <SidebarFooter progress={completedProgress}  total={totalSteps}  prevStep={prevStep} nextStep={nextStep} complete={complete} disableButtons={disableButtons} menuOpen={menuOpen} mobileMenu={mobileMenu} isFinished={isFinished} />
        </div>
    )

}
import { useState, useEffect } from "react";

export default function ToolNavBtn(props) {
    const { logged_in, logout_url, email } = props;
    const [ isOpen, setIsOpen ] = useState(false); 
    useEffect(() => {

    })
    return (
        <div className="fixed top-[10px] right-[10px] z-50">
            <button onClick={() => setIsOpen(!isOpen)} id="trainingNavBtn" className="flex transition bg-darkPurple rounded-full w-[50px] h-[50px]">
                <div className={`trainingNavOpenIcon flex flex-col justify-center items-center gap-y-[6px] rounded-full w-[50px] h-[50px] hover:gap-y-[10px] transition ${(isOpen === true) ? 'hidden' : 'flex'}`}>
                    <div className="w-[20px] h-[2px] bg-white transition rounded-full"></div>
                    <div className="w-[10px] h-[2px] bg-white transition rounded-full"></div>
                    <div className="w-[20px] h-[2px] bg-white transition rounded-full"></div>
                </div>
                
                <div className={`trainingNavCloseIcon  bg-darkPurple flex-col justify-center items-center gap-y-[12px] rounded-full w-[50px] h-[50px] ${(isOpen === true) ? 'flex' : 'hidden'}`}>
                <svg width="30" height="30" viewBox="0 0 30 30" fill="none" xmlns="http://www.w3.org/2000/svg">
                    <rect width="20" height="2" rx="1" transform="matrix(0.707107 -0.707107 -0.707107 -0.707107 8.63604 22.7782)"
                    fill="white" />
                    <rect width="2" height="20" rx="1" transform="matrix(0.707107 -0.707107 -0.707107 -0.707107 21.364 22.7782)"
                    fill="white" />
                </svg>
                </div>
                <ul
                    className={`absolute right-0 top-[60px] bg-white drop-shadow-xl p-[20px] min-w-[240px] rounded-lg ${(logged_in === false) ? 'flex flex-col-reverse' : ''} ${(isOpen === true) ? 'flex' : 'hidden'}`}>
                    

                    </ul>
            </button>
        </div>
  )
}
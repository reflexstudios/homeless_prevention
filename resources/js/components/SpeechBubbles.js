import { useState, useEffect } from "react";


export default function SpeechBubbles(props) {
    const { speech_bubbles} = props;
    const total = speech_bubbles.length;
    return (
        <div className="flex flex-wrap justify-center gap-x-[20px] gap-y-[20px] mt-[40px] -mx-3">
            {speech_bubbles.map(function(row, index){
                const { content, include_bubble_tail } = row;
                return (
                    <div key={'speech-bubbles-' + index} className="bg-white w-full sm:w-[calc(50%-20px)] md:w-[calc(33%-20px)] text-center  rounded-lg relative flex items-center justify-center p-10 overflow-hidden ">
                        <div >
                            <div dangerouslySetInnerHTML={{ __html: content }} />
                    
                            {(include_bubble_tail === true) &&
                                <svg className="absolute right-1/2 translate-x-1/2  -bottom-6" width="22" height="24" viewBox="0 0 22 24" fill="none" xmlns="http://www.w3.org/2000/svg">
                                    <path d="M0.5 0H21.5L0.5 24V0Z" fill="white"/>
                                </svg>
                            }
                        </div>
                    </div>
                )
            })}
        </div>
    )
}


import { useState, useEffect } from "react";

export default function SidebarSection(props) {
    const { title, step, steps, goToStep, section, subtitle, isOpened, onOpen, currentSidebar } = props;
    const [totalMinutes, setTotalMinutes] = useState(0);

    useEffect(() => {
        let totalMinutes = 0;
        steps.map((row) => {
           totalMinutes += row.lesson_length
        });
        setTotalMinutes(totalMinutes);
    })
    return (
        <li className="relative bg-darkestPurple rounded-2xl p-[30px] list-none">
            <button type="button" onClick={onOpen} className="w-full text-left group">
                <div className="flex flex-row items-center justify-between">
                    <div className="h-full ">
                        <p className="text-2xl text-white">{ title }</p>
                        <div className="flex items-center  opacity-50">
                            <svg className="mt-0.5" width="14" height="15" viewBox="0 0 14 15" fill="none" xmlns="http://www.w3.org/2000/svg">
                                <g clipPath="url(#clip0_238_1354)">
                                    <path d="M7 14.5C10.8593 14.5 14 11.3593 14 7.5C14 3.64067 10.8594 0.5 7 0.5C3.14063 0.5 0 3.64067 0 7.5C0 11.3593 3.14067 14.5 7 14.5ZM7 1.43332C10.346 1.43332 13.0667 4.15398 13.0667 7.5C13.0667 10.846 10.346 13.5667 7 13.5667C3.65398 13.5667 0.933318 10.846 0.933318 7.5C0.933318 4.15398 3.65401 1.43332 7 1.43332Z" fill="#fff"/>
                                    <path d="M9.04165 9.73071C9.128 9.8007 9.23063 9.83339 9.33331 9.83339C9.47098 9.83339 9.60631 9.77272 9.69729 9.65839C9.8583 9.45772 9.82561 9.16372 9.62497 9.00271L7.46664 7.27604V3.76671C7.46664 3.51004 7.25665 3.30005 6.99998 3.30005C6.74331 3.30005 6.53333 3.51004 6.53333 3.76671V7.50006C6.53333 7.6424 6.59867 7.77539 6.70832 7.86404L9.04165 9.73071Z" fill="#fff"/>
                                </g>
                                <defs>
                                    <clipPath id="clip0_238_1354">
                                        <rect width="14" height="14" fill="white" transform="translate(0 0.5)"/>
                                    </clipPath>
                                </defs>
                            </svg>
                            <span className="text-white text-2xs ml-2 pt-1 ">{totalMinutes} Minute{(totalMinutes !== 1) ? 's' : ''}</span>
                        </div>
                    </div>

                    <svg className={`transition ${(isOpened === true) ? 'rotate-180' : 'rotate-0'}`} width="10" height="6" viewBox="0 0 10 6" fill="none" xmlns="http://www.w3.org/2000/svg">
                        <path fillRule="evenodd" clipRule="evenodd" d="M9.78033 0.46967C10.0732 0.762563 10.0732 1.23744 9.78033 1.53033L5.78033 5.53033C5.48744 5.82322 5.01256 5.82322 4.71967 5.53033L0.71967 1.53033C0.426777 1.23744 0.426777 0.762563 0.71967 0.46967C1.01256 0.176777 1.48744 0.176777 1.78033 0.46967L5.25 3.93934L8.71967 0.46967C9.01256 0.176777 9.48744 0.176777 9.78033 0.46967Z" fill="#fff"/>
                    </svg>
                </div>
            </button>

            <div className={`relative overflow-hidden transition-all duration-700 ${(isOpened === true) ? 'max-h-96' : 'max-h-0'}`}>
                <div className="text-white flex flex-col gap-y-[20px] mt-[20px]">
                    {steps.map((row, i) => (
                        <span 
                            onClick={() => goToStep(section, i)} key={'sidebar-section-item-' + i} 
                            style={{ opacity: (row.isCompleted === true) ? 0.6 : 1 }}
                        >
                            <input onChange={() => {}} type="checkbox" className="cursor-pointer" checked={(row.isCompleted === true) ? "checked" : ""} />
                            <label className={`ml-2 text-white text-1xs cursor-pointer ${((currentSidebar === true) && (i === step)) ? 'underline' : ''}`}>
                                { row.title } 
                            </label>
                        </span>
                    ))}
                </div>
            </div>
        </li>
  )
}
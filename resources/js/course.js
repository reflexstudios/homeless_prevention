import { useState, useEffect, useRef } from "react";
import ReactDOM from 'react-dom';
import { v4 as uuidv4 } from 'uuid';

import HeroBanner from './components/HeroBanner';
import Sidebar from './components/Sidebar';
import Splash from './components/Splash';
import GeneralContent from "./components/GeneralContent";
import Finish from "./components/Finish";

const axios = require('axios');

export default function Course() {
  const [step, setStep] = useState(0);
  const [newSection, setNewSection] = useState(false);
  const [section, setSection] = useState(0);
  const [currentStep, setCurrentStep] = useState(0);
  const [totalSteps, setTotalSteps] = useState(0);
  const [totalLength, setTotalLength] = useState(0);
  const [isLoaded, setIsLoaded] = useState(false);
  const [isLoggedIn, setIsLoggedIn] = useState(false);
  const [isStart, setIsStart] = useState(true);
  const [isFinish, setIsFinish] = useState(false);
  const [isLoggedInError, setIsLoggedInError] = useState(false);
  const [isCertificateSent, setIsCertificateSent] = useState("");
  const [isFinished, setIsFinished] = useState("");
  const [savedQuestions, setSavedQuestions] = useState("");
  const [isRegError, setIsRegError] = useState(false);
  const [userProfile, setUserProfile] = useState({});
  const [sections, setSections] = useState([]);
  const [data, setData] = useState({});
  const [organisations, setOrganisations] = useState([]);
  const [sectors, setSectors] = useState([]);
  const [heroBanner, setHeroBanner] = useState("");
  const [parentTitle, setParentTitle] = useState("");
  const [courseTitle, setCourseTitle] = useState("");
  const [courseDesc, setCourseDesc] = useState("");
  const [title, setTitle] = useState("");
  const [titleLine1, setTitleLine1] = useState("");
  const [titleLine2, setTitleLine2] = useState("");
  const [generalContent, setGeneralContent] = useState([]);
  const [questionnaire, setQuestionnaire] = useState([]);
  const [activeSidebarIndex, setActiveSidebarIndex] = useState(0);
  const [disableButtons, setdisableButtons] = useState(false);
  

  function submitQuestionnaire() {
    var uuid = localStorage.getItem("entry_id");
    let questionnaire_answer = "";
  
    console.log("BEFPREquestionnaire (before savedQuestions):", questionnaire);
    if (savedQuestions) {
      questionnaire_answer += savedQuestions + "\n\n";
    }
  
    console.log("questionnaire_answer:", questionnaire_answer);
  
    console.log("BEFPREquestionnaire (before forEach):", questionnaire);
    questionnaire
      .filter((row) => row) // Filter out empty slots
      .forEach((row, key) => {
        console.log("Current row (key):", key);
        console.log("Row data:", row);
  
        let currentStepLogged = false;
  
        row.forEach((item, index) => {
          console.log("item, index:", item, index);
          if (!currentStepLogged) {
            questionnaire_answer += "Questionnaire " + item["currentStep"] + ":\n";
            currentStepLogged = true;
          }
  
          console.log(" - " + item["question"] + ": " + item["answer"] + "\n");
          questionnaire_answer += " - " + item["question"] + ": " + item["answer"] + "\n";
        });
        questionnaire_answer += "\n";
      });
  
    console.log("BEFPREquestionnaire (after forEach):", questionnaire);
  
    var enquireFormData = new FormData();
    enquireFormData.append("uuid", uuid);
  
    if (userProfile.id) {
      enquireFormData.append("user_id", userProfile.id);
      enquireFormData.append("user", userProfile.name);
    }
  
    enquireFormData.append("questionnaire", questionnaire_answer);
  
    console.log("enquireFormData: ", questionnaire_answer);
  
    axios
      .post("../../forms/course", enquireFormData, {
        headers: {
          "X-Requested-With": "XMLHttpRequest",
        },
      })
      .then((response) => {
        console.log("FORM RESPONSE");
        console.log(response.data);
      });
  
    setdisableButtons(false);
  }
  

  function captureQuestionnaire(key, index, currentStep, question, answer) {
    console.log('key:', key);
    console.log('index:', index);
    console.log('currentStep:', currentStep);
    console.log('question:', question);
    console.log('answer:', answer);
  
    let current = questionnaire;
  
    if (!(key in current)) {
      current[key] = [];
    }
    
    // Initialize the array element if it's undefined
    if (!current[key][index]) {
      current[key][index] = {
        'currentStep': currentStep,
        'question': question,
        'answer': answer
      };
    } else {
      current[key][index]['answer'] = answer;
    }
  
    setQuestionnaire(current);
  }
  
  
  
  

  function getDataStatamic(data, course_slug) {
    const {page, children} = data;
    let tmp = [];
    let counter = 0;
    let active_section = 0;
    let active_entry = 0;
    let total_length = 0;

    setCourseTitle(page.title);
    setCourseDesc(page.course_description);

    for (const [key, section] of Object.entries(children)) {
      tmp[key] = { title: section.page.title, steps: [] };
      for (const [stepId, stepRow] of Object.entries(section.children)) {
        
        const lesson_length = ((stepRow.page.lesson_length !== null) && (stepRow.page.lesson_length !== undefined)) ? stepRow.page.lesson_length : 0;
        total_length += lesson_length;
        tmp[key].steps[stepId] = { title: stepRow.page.title, subtitle: stepRow.page.title, lesson_length, slug: stepRow.page.slug, isCompleted: false };
        if(stepRow.page.slug === course_slug) {
          active_section = key;
          active_entry = stepId;
          setCurrentStep(counter);
          setSection(key);
        }
        counter++;
      }
    }
    setTotalSteps(counter);
    setSections(tmp);
    setTotalLength(total_length);

    const { hero_banner, title, title_line_1, title_line_2, training_tool_general_content } = children[active_section].children[active_entry].page;
    setHeroBanner(hero_banner.url);
    setParentTitle(page.title);
    setTitle(title);
    setTitleLine2(title_line_2);
    setTitleLine1(title_line_1);
    setGeneralContent(training_tool_general_content);
    setData(data);
  }

  async function init() {
    const course = document.getElementById('course');
    const slug = course.getAttribute('data-course-slug');
    const login = course.getAttribute('data-login');
    const user_id = course.getAttribute('data-user-id');
    const token = course.getAttribute('data-csrf-token');
    const user_logout = course.getAttribute('data-user-logout-url');
    const course_start = course.getAttribute('data-course-start');
    const prev = localStorage.getItem("entry_id");

    setIsLoggedIn((login === 'true') ? true : false);

    // check profile
    if(login === 'true') {
      const res = await fetch('/api/users/' + user_id); // Get the data from the API
      const { data } = await res.json() // Convert it to JSON
      setUserProfile(data);
    }


    // course api
    axios.get('/api/collections/courses/tree').then(function (response) {
      // handle success
      const filtered = response.data.data.filter(row => row.page.slug === slug);
      //data = filtered[0];
      getDataStatamic(filtered[0], course_start);

      if(user_id !== '') {
        let savedUserID = '';
        
        axios.get("../../forms/get-user-course?uuid=&user_id=" + user_id , {
          headers: {
            'X-Requested-With': 'XMLHttpRequest',
          }
        })
        .then(response => {
            let {progress} = response.data;
            if((progress !== undefined) && (progress !== null)) {
              console.log('FOUND USER1: ', response.data);
              setIsFinished(response.data.is_completed);
              setSavedQuestions(response.data.questionnaire);
              savedUserID = response.data.user_id;
              const obj = JSON.parse(progress);
              let active_slug = '';
              let active_title = '';
              let active_sidebar = -1;
              let active_step_id = 0;
              // top level sections
              for (const [parentKey, parentValue] of Object.entries(obj)) {
                for (const [stepId, stepRow] of Object.entries(obj[parentKey].steps)) {
                  if(obj[parentKey].steps[stepId].isCompleted === false) {
                    // failed step
                    if(active_slug === '') {
                      active_slug = obj[parentKey].steps[stepId].slug;
                      active_title = obj[parentKey].steps[stepId].title;
                      active_sidebar = parseInt(parentKey);
                      active_step_id = parseInt(stepId);
                    }
                  }
                }
              }
              setStep(active_step_id);
              setActiveSidebarIndex(active_sidebar);
              if(active_slug !== '') {
                getDataStatamic(filtered[0], active_slug);
              }
              setSections(obj);
              window.history.pushState('', active_title, slug);
            }
        });

        

        if (!savedUserID){

          axios.get("../../forms/get-user-course?uuid=" + prev + "&user_id=", {
            headers: {
              'X-Requested-With': 'XMLHttpRequest',
            }
          })
          .then(response => {
              let {progress} = response.data;
              if((progress !== undefined) && (progress !== null)) {
                console.log('FOUND USER1: ', response.data);
                setIsFinished(response.data.is_completed);
                setSavedQuestions(response.data.questionnaire);
                const obj = JSON.parse(progress);
                let active_slug = '';
                let active_title = '';
                let active_sidebar = -1;
                let active_step_id = 0;
                // top level sections
                for (const [parentKey, parentValue] of Object.entries(obj)) {
                  for (const [stepId, stepRow] of Object.entries(obj[parentKey].steps)) {
                    if(obj[parentKey].steps[stepId].isCompleted === false) {
                      // failed step
                      if(active_slug === '') {
                        active_slug = obj[parentKey].steps[stepId].slug;
                        active_title = obj[parentKey].steps[stepId].title;
                        active_sidebar = parseInt(parentKey);
                        active_step_id = parseInt(stepId);
                      }
                    }
                  }
                }
                setStep(active_step_id);
                setActiveSidebarIndex(active_sidebar);
                if(active_slug !== '') {
                  getDataStatamic(filtered[0], active_slug);
                }
                setSections(obj);
                window.history.pushState('', active_title, slug);
              }
          });

          var courseFormData = new FormData();
          courseFormData.append('uuid', prev);
          courseFormData.append('user_id', user_id);
          axios.post("../../forms/course", courseFormData, {
            headers: {
              'X-Requested-With': 'XMLHttpRequest',
            }
          })
          .then(response => {
              console.log('courseFormData update:');
              console.log(response.data);
              
          });
        }

        

      }else if((prev !== null) && (prev !== undefined))  {
        console.log("found prev, prev:" + prev);
        axios.get("../../forms/get-user-course?uuid=" + prev + "&user_id=" , {
          headers: {
            'X-Requested-With': 'XMLHttpRequest',
          }
        })
        
        .then(response => {
            let {progress} = response.data;
            
            if((progress !== undefined) && (progress !== null)) {
              console.log('FOUND USER2: ', response.data);
              
              if(response.data.uuid) {
                var uuid = response.data.uuid;
                localStorage.setItem("entry_id", uuid);
              }
              setSavedQuestions(response.data.questionnaire);
              console.log('questionnaire2: ', response.data.questionnaire);
              setIsFinished(response.data.is_completed);
              console.log('isFinished2: ', isFinished);
              const obj = JSON.parse(progress);
              //console.log("obj: ", obj);


              let active_slug = '';
              let active_title = '';
              let active_sidebar = -1;
              let active_step_id = 0;
              
              // top level sections
              for (const [parentKey, parentValue] of Object.entries(obj)) {
                for (const [stepId, stepRow] of Object.entries(obj[parentKey].steps)) {
                  if(obj[parentKey].steps[stepId].isCompleted === false) {
                    // failed step
                    if(active_slug === '') {
                      active_slug = obj[parentKey].steps[stepId].slug;
                      active_title = obj[parentKey].steps[stepId].title;
                      active_sidebar = parseInt(parentKey);
                      active_step_id = parseInt(stepId);
                    }
                  }
                }
              }

              //setCurrentStep(active_sidebar);
              setStep(active_step_id);
              setActiveSidebarIndex(active_sidebar);

              if(active_slug !== '') {
                getDataStatamic(filtered[0], active_slug);
              }

              setSections(obj);
              window.history.pushState('', active_title, slug);
            }

            //setIsFinish(true);

            //setIsStart(false);
        });
      
      }
    })

    axios.get('/api/collections/organisations/entries').then(function (response) {
      const organisations = response.data.data;
      setOrganisations(organisations)
    })

    axios.get('/api/globals/sectors').then(function (response) {
      const sectors = response.data.data;
      setSectors(sectors)
    })

    

    .catch(function (error) {
      // handle error
      console.log('LOAD ERROR');
      console.log(error);
    })
    .then(function () {
      // always executed
    });
  }

  async function getStarted() {
      // create form
      var prev = localStorage.getItem("entry_id");
      console.log('PREV:: ', prev);
      console.log('isFinished: ', isFinished);
      if((prev === null) || (prev === undefined)) {
          const { title, slug } = data.children[0].children[0].page;
          window.history.pushState('', title, slug);

          const course = document.getElementById('course');
          const token = course.getAttribute('data-csrf-token');

          var enquireFormData = new FormData();
          
          var uuid = uuidv4();
          localStorage.setItem("entry_id", uuid);
          enquireFormData.append('uuid', uuid);
          enquireFormData.append('is_completed', 'No');
          enquireFormData.append('progress', JSON.stringify(sections));
        
          axios.post("../../!/forms/course_entries", enquireFormData, {
            headers: {
              'X-CSRF-Token': token,
              'X-Requested-With': 'XMLHttpRequest',
            }
          }) 

          .then(response => {
              console.log(response.data);
              setIsStart(false);
          });
      } else {
        // previous user
        setIsStart(false);
      }



  }

  function onLogin(email, password) {
    if(isLoggedInError === true) {
      setIsLoggedInError(false);
    }
    const course = document.getElementById('course');
    const token = course.getAttribute('data-csrf-token');

    axios.post("../../forms/login", {
      email, password
    }, {
      headers: {
        'X-CSRF-Token': token,
        'Content-Type': 'application/json',
      }
    })
    .then(response => {
      console.log('RESONSE: ', response);
      if(response.data.id) {
        setUserProfile(response.data);
        setIsLoggedIn(true);
      } else {
        setIsLoggedInError(true);
      }
    });

  }

  function goToStep(section, entry) {
    /* save previous step */
    let updatedCompleted = sections;
    updatedCompleted[section]['steps'][step].isCompleted = true;
    saveStepProgress(updatedCompleted);
    setSections(updatedCompleted);

    const { 
      title, title_line_1, title_line_2, hero_banner, 
      training_tool_general_content, slug 
    } = data.children[section].children[entry].page;
    setTitle(title);
    setTitleLine2(title_line_2);
    setTitleLine1(title_line_1);
    setGeneralContent(training_tool_general_content);
    setHeroBanner(hero_banner.url);
    //setStep(data.children[section].children.length);
    setSection(section);
    window.history.pushState('', title, slug);

    let counter = 0;
    for (const [key, section] of Object.entries(data.children)) {
      for (const [stepId, stepRow] of Object.entries(section.children)) {
        if(stepRow.page.slug === slug) {
          setCurrentStep(counter);
          setSection(key);
        }
        counter++;
      }
    }


    window.scrollTo(0, 0);
  }

  function prevStep() {
    //let updatedCompleted = sections;
    //updatedCompleted [section]['steps'][step].isCompleted = false;
    //setSections(updatedCompleted);
    const prev = step - 1;
    const updated = currentStep - 1;

    console.log("step: ", step)
    console.log("prev: ", prev)
    console.log("currentStep: ", currentStep)
    console.log("updated: ", updated)
    console.log("newSection: ", newSection)


    setCurrentStep(updated);
    if(prev < 0) {
      console.log("new section")
      if((section - 1) >= 0) {
      
        const newStep = data.children[section - 1].children.length - 1 ;
        console.log("newStep", newStep)
        const { 
          title, title_line_1, title_line_2, hero_banner, 
          training_tool_general_content, slug 
        } = data.children[section - 1].children[newStep].page;


        setStep(data.children[section - 1].children.length - 1);
        setTitle(title);
        setTitleLine2(title_line_2);
        setTitleLine1(title_line_1);
        setGeneralContent(training_tool_general_content);
        setHeroBanner(hero_banner.url);
        
        setSection(section-1);
        setActiveSidebarIndex(parseInt(section) - 1);
        console.log("step: ", step)
        window.history.pushState('', title, title_line_1, title_line_2, slug);
      }
      console.log("not new section but prev < 0")
    } else {
      console.log("standard section")
      const { 
        title, title_line_1, title_line_2, hero_banner, 
        training_tool_general_content, slug 
      } = data.children[section].children[prev].page;
      setTitle(title);
      setTitleLine2(title_line_2);
      setTitleLine1(title_line_1);
      setGeneralContent(training_tool_general_content);
      setHeroBanner(hero_banner.url);
      setStep(prev);
      window.history.pushState('', title, title_line_1, title_line_2, slug);
    }

    window.scrollTo(0, 0);
  }

  function disablenext() {
    setdisableButtons(true);
  }



  function saveStepProgress(completed) {
    var uuid = localStorage.getItem("entry_id");
    var user_id_local = localStorage.getItem("user_id_local");

    //console.log('SAVED LOCAL USERID: ' + user_id_local);


      var enquireFormData = new FormData();
      enquireFormData.append('uuid', uuid);
      enquireFormData.append('progress', JSON.stringify(completed));
      axios.post("../../forms/course", enquireFormData, {
        headers: {
          'X-Requested-With': 'XMLHttpRequest',
        }
      })
      .then(response => {
          console.log(response.data);
      });
  }

  function nextStep() {
    let updatedCompleted = sections;
    updatedCompleted[section]['steps'][step].isCompleted = true;

    saveStepProgress(updatedCompleted);

    setSections(updatedCompleted);
    const next = step + 1;
    const updated = currentStep + 1;
    setCurrentStep(updated);
    if((data.children[section].children.length) == next) {
      if(data.children[parseInt(section)+1]) {
        const { 
          title, title_line_1, title_line_2, hero_banner, 
          training_tool_general_content, slug 
        } = data.children[parseInt(section)+1].children[0].page;
        setTitle(title);
        setTitleLine2(title_line_2);
        setTitleLine1(title_line_1);
        setActiveSidebarIndex(parseInt(section) + 1);
        setGeneralContent(training_tool_general_content);
        setHeroBanner(hero_banner.url);
        setStep(0);
        setSection(parseInt(section)+1);
        window.history.pushState('', title, title_line_1, title_line_2, slug);
      }
    } else {
        const { 
          title, title_line_1, title_line_2, hero_banner, 
          training_tool_general_content, slug 
        } = data.children[section].children[next].page;
        setTitle(title);
        setTitleLine2(title_line_2);
        setTitleLine1(title_line_1);
        setHeroBanner(hero_banner.url);
        setGeneralContent(training_tool_general_content);
        setStep(next);
        // update url
        window.history.pushState('', title, title_line_1, title_line_2, slug);
    }

    window.scrollTo(0, 0);
  }

  

  const signUpUser = ({firstName, surname, sector, email, password, confirmPassword}) => {
    setIsRegError(true);
    console.log("signup found ");
    var uuid = localStorage.getItem("entry_id");
    
    let isUserId = "";
    let isUserName = "";

    console.log('isUserId 1: ', isUserId);
    console.log('isUserName 1: ', isUserName);

    var enquireFormData = new FormData();
    enquireFormData.append('first_name', firstName);
    enquireFormData.append('last_name', surname);
    enquireFormData.append('sector', sector);
    enquireFormData.append('email', email);
    enquireFormData.append('password', password);
  
    axios.post("../../forms/user/create", enquireFormData, {
      headers: {
        'X-Requested-With': 'XMLHttpRequest',
      }
    })
    .then(response => {
        if(response.data.error) {
          setIsRegError(true);
        } else {
          setIsRegError(false);
          setUserProfile(response.data);
          console.log("userdata set ");
        }
    });

    setTimeout(() => {
      console.log("after 1");
      axios.get('/api/users/?filter[email:contains]='+ email).then(function (response) {
        const data = response.data.data;
        console.log('data: ', data);

        const token = course.getAttribute('data-csrf-token');

        isUserId = ('data ID: ', data[0].id);
        isUserName = ('data ID: ', data[0].name);

        console.log('isUserId 2: ', isUserId);
        console.log('isUserName 2: ', isUserName);

        var courseFormData = new FormData();
        courseFormData.append('uuid', uuid);
        courseFormData.append('user_id', isUserId);
        courseFormData.append('user', isUserName);

        console.log('courseFormData: ', courseFormData);
        axios.post("../../forms/course", courseFormData, {
          headers: {
            'X-Requested-With': 'XMLHttpRequest',
          }
        })
        .then(response => {
            console.log('courseFormData update:');
            console.log(response.data);
            
        });

      })
    }, 1000);




  }

  

  const complete = () => {
    setIsFinish(true);
    var uuid = localStorage.getItem("entry_id");
    var enquireFormData = new FormData();
    enquireFormData.append('uuid', uuid);
    enquireFormData.append('is_completed', 'Yes');
  
    localStorage.setItem("entry_id", uuid);

    console.log('enquireFormData: ', enquireFormData);

    axios.post("../../forms/course", enquireFormData, {
      headers: {
        'X-Requested-With': 'XMLHttpRequest',
      }
    })
    .then(response => {
        console.log('FORM RESPONSE (complete):');
        console.log(response.data);
        
    });
  }

  const saveCert = () => {
    const course = document.getElementById('course');
    const token = course.getAttribute('data-csrf-token');
    setIsCertificateSent('loading');
    console.log('SAVE CERTIFICATE: ', userProfile);
    
    var enquireFormData = new FormData();
    enquireFormData.append('name', userProfile.name);
    enquireFormData.append('course', courseTitle);
    enquireFormData.append('email', userProfile.email);

    axios.post("../../!/forms/certificates", enquireFormData, {
      headers: {
        'X-CSRF-Token': token,
        'X-Requested-With': 'XMLHttpRequest',
      }
    })
    .then(response => {
        console.log('FORM RESPONSE (save cert): ');
        console.log(response.data);
        setIsCertificateSent('true');
    });
  }

  useEffect(() => {
    if(isLoaded === false) {
        setIsLoaded(true);
        init();
    }
  })
  
  return (
    <>
    {
      (isFinish === true) 
      ? 
        <Finish
          user={userProfile}
          signUp={signUpUser}
          saveCert={saveCert}
          onLogin={onLogin} 
          isLoggedInError={isLoggedInError}
          isCertificateSent={isCertificateSent}
          isRegError={isRegError}
          title={courseTitle}
          sectors={sectors}
        />
      :
        <>    
          {(isStart === true) && 
            <Splash 
              isLoggedIn={isLoggedIn}
              getStarted={getStarted}
              totalLength={totalLength}
              courseTitle={courseTitle}
              courseDesc={courseDesc}
              isFinished={isFinished}
              complete={complete}
            />
            }
          {(isStart === false) && 
            <>  
              <Sidebar 
                goToStep={goToStep}
                complete={complete}
                title={parentTitle}
                sections={sections}
                isFinished={isFinished}
                currentStep={currentStep}
                step={step}
                totalSteps={totalSteps}
                prevStep={prevStep}
                nextStep={nextStep}
                disableButtons={disableButtons}
                activeSidebarIndex={activeSidebarIndex}
                setActiveSidebarIndex={setActiveSidebarIndex}
              />
              <HeroBanner hero_banner={heroBanner} title={title} title_line_1={titleLine1} title_line_2={titleLine2} />
              <GeneralContent 
                items={generalContent} 
                isFinished={isFinished}
                captureQuestionnaire={captureQuestionnaire} 
                submitQuestionnaire={submitQuestionnaire} 
                organisations={organisations}  
                disablenext={disablenext} 
                currentStep={currentStep}
                />
            </>
          }
        </>
    }
    </>
  );
}


if (document.getElementById('course')) {
  ReactDOM.render(<Course />, document.getElementById('course'));
}
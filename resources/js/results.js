import { createApp, VueElement } from 'vue';
import App from './App.vue';
const app = createApp(App).mount("#app");

<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Statamic\Facades\Form;
use Statamic\Contracts\Forms\Submission;
use Statamic\Events\FormSubmitted;
use Statamic\Events\SubmissionCreated;
use Statamic\Facades\User;
use Laravel\Socialite\Contracts\User as SocialiteUser;
use Statamic\Events\UserRegistered;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;


class LoginUserController extends Controller
{
    //

    public function index(Request $request) {
        $response = [];
        $token = csrf_token();


        if (Auth::attempt($request->only('email', 'password'))) {
            $response = ['id' => Auth::id(), 'name' => Auth::user()->name, 'email' => Auth::user()->email];
        } else {
            $response = ['error' => 'Login failed'];
        }

        return json_encode($response);
    }

}

<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Statamic\Facades\Form;
use Statamic\Contracts\Forms\Submission;
use Statamic\Events\FormSubmitted;
use Statamic\Events\SubmissionCreated;
use Statamic\Facades\User;
use Laravel\Socialite\Contracts\User as SocialiteUser;
use Statamic\Events\UserRegistered;
use Illuminate\Support\Facades\Auth;


class CreateUserController extends Controller
{
    //

    public function index(Request $request) {
        $query = User::query();
        $exists = $query->where('email', $request->email)->count();
        $token = csrf_token();

        if($exists === 0) {
            $values = $request->all();

            $user = User::make()
                ->email($request->email)
                ->password($request->password)
                ->data($values);
    
            $save = $user->save();
    
            UserRegistered::dispatch($user);
            Auth::login($user);

            return ["name" => $values['first_name'].' '.$values['last_name'], "email" => $values['email']];
        } else {
            return ["error" => "User Exists"];
        }



    }

}

<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Statamic\Facades\Form;
use Statamic\Contracts\Forms\Submission;
use Statamic\Events\FormSubmitted;
use Statamic\Events\SubmissionCreated;

use Statamic\Support\Str;

class GetFormController extends Controller
{
    //

    public function index(Request $request) {
        $form = Form::find('course_entries');
        $submissions = $form->submissions();
        $values = $request->all();

        $selected = [];
        if($submissions->count() > 0) {
            foreach($submissions as $key=>$row) {
                $data = $row->data();

                if(isset($data['uuid'])) {
                    if((
                        ($values['uuid'] !== null) && ($data['uuid'] === $values['uuid'])
                        ) 
                        || 
                        (
                            ($values['user_id'] !== '') && (isset($data['user_id'])) && ($data['user_id'] === $values['user_id'])
                        )
                    ) {
                        $selected = $row;
                    }
                }
            }
        }
        return $selected;
    }

}

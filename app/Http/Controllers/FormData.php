<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Http;

use Statamic\Facades\Form;
use Statamic\Contracts\Forms\Submission;

class FormData extends Controller
{
    public function index(Request $request)
    {
        $form = Form::find('course_entries');
        $submissions = $form->submissions();
        $startDate = $request->input('start_date');
        $endDate = $request->input('end_date');

        $apiUrl = 'https://homelessnesspreventionforum.com/api/collections/courses/entries/a8ef0a5e-7a97-4141-967c-b8157a595b21'; // Replace with the URL of your API endpoint
        $response = Http::get($apiUrl);
        $questionData = $response->json();

        if ($startDate && $endDate) {
            $startDate = \Carbon\Carbon::parse($startDate);
            $endDate = \Carbon\Carbon::parse($endDate);

            $submissions = $submissions->filter(function ($submission) use ($startDate, $endDate) {
                $submissionDate = \Carbon\Carbon::parse($submission->date());
                $submissionDate = $submissionDate->setTimezone('UTC'); // Ensuring that the timezone is set to UTC
                return $submissionDate->between($startDate, $endDate);
            });
        }

        // Apply the formatSubmission function to all submissions
        $formattedSubmissions = $submissions->map(function ($submission) {
            return $this->formatSubmission($submission);
        });

        $totalEntries = count($formattedSubmissions);
        $totalCompleted = count($formattedSubmissions->where('is_completed', 'Yes'));

        $filteredSubmissions = $formattedSubmissions->filter(function ($submission) {
            $isCompleted = $submission['is_completed'] === 'Yes';
            //$noAnswerBefore = !isset($submission['questionnaire']['before']['I know the main reasons why people become homeless']);
            $noAnswerAfter = !isset($submission['questionnaire']['after']['I know the main reasons why people become homeless']);
        
            // return $isCompleted && $noAnswerBefore && $noAnswerAfter;
            return $isCompleted && $noAnswerAfter;
        });

        // Retrieve the training_tool_general_content array from the $questionData
        $trainingToolGeneralContent = $questionData['data']['training_tool_general_content'][1]['questions'];

        $optionChoices = $this->extractOptionChoices($questionData['data']['training_tool_general_content'][1]['questions']);

        // Initialize the question mapping based on the question texts
        $questionMapping = $this->initQuestionMapping($trainingToolGeneralContent);

        // Aggregate the data
        $resultsData = $questionMapping;
        foreach ($formattedSubmissions as $submission) {
            if ($submission['is_completed'] === 'Yes') {
                foreach ($submission['questionnaire'] as $questionnaireSection => $questions) {
                    foreach ($questions as $question => $answer) {
                        // Increment the count for the answer in the respective section
                        $resultsData[$question][$questionnaireSection][$answer] = ($resultsData[$question][$questionnaireSection][$answer] ?? 0) + 1;

                        // Increment the total count for the respective section
                        $resultsData[$question][$questionnaireSection]['Total']++;
                    }
                }
            }
        }

        return [
            'total_entries' => $totalEntries,
            'total_completed' => $totalCompleted,
            'results_data' => $resultsData,
            'filtered_submissions' => $filteredSubmissions,
        ];
    }



 // Add a new function to initialize the question mapping based on the question texts
 private function initQuestionMapping(array $questionData)
 {
     $questionMapping = [];
     foreach ($questionData as $item) {
         if (isset($item['question'])) {
             $questionText = $item['question'];
             $questionMapping[$questionText] = [
                 'before' => [],
                 'after' => [],
                 'label_left' => $item['label_left'],
                 'label_right' => $item['label_right'],
             ];
             if (isset($item['option_choices'])) {
                 foreach ($item['option_choices'] as $choice) {
                     $questionMapping[$questionText]['before'][$choice['option']] = 0;
                     $questionMapping[$questionText]['after'][$choice['option']] = 0;
                 }
             }
             // Add the 'Total' key with a value of 0 in the 'before' and 'after' arrays
             $questionMapping[$questionText]['before']['Total'] = 0;
             $questionMapping[$questionText]['after']['Total'] = 0;
         }
     }
     return $questionMapping;
 }
 




private function findQuestionLabels(array $questionData, string $questionText)
{
    foreach ($questionData as $item) {
        if (isset($item['question']) && $item['question'] === $questionText) {
            return [
                'label_left' => $item['label_left'],
                'label_right' => $item['label_right'],
            ];
        }
    }

    return ['label_left' => null, 'label_right' => null];
}

private function extractOptionChoices(array $questionData)
{
    $choices = [];

    foreach ($questionData as $item) {
        if (isset($item['option_choices'])) {
            foreach ($item['option_choices'] as $choice) {
                $choices[] = $choice['option'];
            }
            break;
        }
    }

    return $choices;
}







// A separate function to format the submission data
private function formatSubmission(Submission $submission) {
    $data = $submission->data();
    $questionnaire = $data['questionnaire'] ?? null;
    $questionnaireData = []; // Initialize the variable as an empty array

    if ($questionnaire) {
        $questionnaireLines = explode("\r\n", $questionnaire);
        $currentQuestionnaire = 'before';
        $questionnaireCount = 0;

        foreach ($questionnaireLines as $line) {
            if (strpos($line, "Questionnaire") !== false) {
                $questionnaireCount++;
                // Change the value of $currentQuestionnaire when it's the second occurrence of "Questionnaire"
                if ($questionnaireCount === 2) {
                    $currentQuestionnaire = 'after';
                }
            } else {
                $parts = explode(": ", $line);
                if (count($parts) === 2) {
                    list($question, $answer) = $parts;
                    $cleanQuestion = trim(str_replace(" - ", "", $question));
                    
                    // Check for the specific question and handle "0" value properly
                    if ($cleanQuestion === "Indicate below the number of Floating Support Services you know of" && $answer === "0") {
                        $questionnaireData[$currentQuestionnaire][$cleanQuestion] = "0";
                    } else {
                        // Only add the answer if it's not empty
                        if (!empty($answer)) {
                            $questionnaireData[$currentQuestionnaire][$cleanQuestion] = trim($answer);
                        }
                    }
                } else {
                    // Log the issue for improperly formatted entries
                    error_log("Improperly formatted entry: " . $line);
                }
            }
        }
    }

    return [
        'uuid' => $submission->id(),
        'user_id' => $data['user_id'] ?? null,
        'user' => $data['user'] ?? null,
        // Add all other fields you want to include
        'questionnaire' => $questionnaireData,
        'is_completed' => $data['is_completed'] ?? null,
        // Remove the progress field
        //'progress' => $data['progress'] ?? null,
        'id' => $data['id'] ?? null,
        'date' => $data['date'] ?? null,
    ];
}







}

<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Statamic\Facades\Form;
use Statamic\Contracts\Forms\Submission;
use Statamic\Events\FormSubmitted;
use Statamic\Events\SubmissionCreated;

use Statamic\Support\Str;

class UpdateFormController extends Controller
{
    //

    public function index(Request $request) {
        $form = Form::find('course_entries');
        $submissions = $form->submissions();
        $values = $request->all();

        error_log(print_r($values, true));

        if($submissions->count() > 0) {
            $selected = [];
            foreach($submissions as $key=>$row) {
                if($row->data()) {
                    $data = $row->data();
                    if(isset($data['uuid'])) {
                        if($data['uuid'] === $values['uuid']) {
                            $selected = $row;
                            if(($selected->data() !== null) && ($selected->data()['uuid'])) {
                                if(isset($values['progress'])) {
                                    // completed
                                    $update = array_merge($selected->data(), ['progress' => $values['progress']]);
                                    $selected->data($update);
                                    $selected->save();
                                    error_log(print_r($selected->data(), true));
                                } elseif(isset($values['is_completed'])) {
                                    // completed
                                    $update = array_merge($selected->data(), ['is_completed' => $values['is_completed']]);
                                    $selected->data($update);
                                    $selected->save();
                                    error_log(print_r($selected->data(), true));
                                } elseif(isset($values['questionnaire'])) {
                                    //questionnaire
                                    $update = array_merge($selected->data(), ['questionnaire' => $values['questionnaire']]);
                                    $selected->data($update);
                                    $selected->save();
                                    error_log(print_r($selected->data(), true));
                                } elseif(isset($values['user_id'])) {
                                    // completed
                                    $update = array_merge($selected->data(), ['user_id' => $values['user_id']]);
                                    $selected->data($update);
                                    $selected->save();
                                    error_log(print_r($selected->data(), true));
                                
                                } elseif(isset($values['user'])) {
                                    // completed
                                    $update = array_merge($selected->data(), ['user' => $values['user']]);
                                    $selected->data($update);
                                    $selected->save();
                                    error_log(print_r($selected->data(), true));
                                }
                            }
                        }
                    }
                }
            }

        }


        return true;
    }

}

---
id: 121f2487-3677-420a-9f51-7780a06dea70
blueprint: organisations
title: 'Ark Housing Association'
logo: logos/ARK_Housing.png
target_group: 'Families experiencing, or at risk of homelessness.  Greater Belfast area.'
referral_source: 'Referrals can come from a range of professionals and organisations.  Self-referrals are also accepted.'
replicator:
  -
    name: 'Ark Housing Association'
    email: homelessservices@arkhousing.co.uk
    phone_number: '02890 752310'
    location: Belfast
    type: contact
    enabled: true
website: "www.arkhousing.co.uk \_"
use_meta_keywords: false
no_index_page: false
no_follow_links: false
sitemap_priority: '0.5'
sitemap_changefreq: daily
override_twitter_settings: false
override_twitter_card_settings: false
twitter_card_type_page: summary
updated_by: cb226d0f-4883-4544-9237-a7b44b67c7dc
updated_at: 1648616085
---

---
id: ce6ebfde-935d-4959-8ba4-398d412f154c
blueprint: organisations
title: NIACRO
logo: logos/NIACRO.png
target_group: |-
  Anyone with their own tenancy that is at risk due to ASB in Northern Ireland

  Anyone effected by hate crime in Greater Belfast.
referral_source: |-
  NIHE
  Social Landlords
  Private Landlords
  Home owners
  Self
replicator:
  -
    name: NIACRO
    email: apacfs@niacro.co.uk
    phone_number: '028 90320157'
    location: 'Northern Ireland'
    type: contact
    enabled: true
website: 'https://www.niacro.co.uk/'
use_meta_keywords: false
no_index_page: false
no_follow_links: false
sitemap_priority: '0.5'
sitemap_changefreq: daily
override_twitter_settings: false
override_twitter_card_settings: false
twitter_card_type_page: summary
updated_by: cb226d0f-4883-4544-9237-a7b44b67c7dc
updated_at: 1648617231
---

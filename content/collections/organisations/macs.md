---
id: 4088ddbc-f4c0-4c92-88c2-ac531774dd84
blueprint: organisations
title: MACS
logo: logos/macs-logo.webp
target_group: |-
  MACS Supporting Children and Young People
  Floating Support Service

  If you are aged 16-25 and are homeless, at risk of homelessness or need help maintaining your tenancy, Floating Support can help you.
  We offer Floating Support in the following areas:

  Belfast Floating Support
  MACS Belfast covers the geographical areas of North, South, East and West 
  Belfast.

  Lisburn Floating Support
  MACS Lisburn covers the Colin area of Belfast, Dundonald and Lisburn.

  Downpatrick Floating Support
  MACS Downpatrick covers the geographical  areas of Downpatrick, Clough, Killyleagh, Ardglass, Strangford, Shrigley, Crossgar, Ballynahinch, Drumaness, Castlewellan, Dundrum and Newcastle. 

  Newry Floating Support
  MACS Newry covers the Newry Area.
referral_source: |-
  We accept referrals from a range of services/organisations and young people can also self-refer. Support is on a voluntary basis and the young person must want to engage with the service.
  You can send a completed referral form to the following email address:
replicator:
  -
    name: 'Belfast Area'
    email: belfastfloatingsupport@macsni.org
    location: Belfast
    type: contact
    enabled: true
  -
    name: 'Lisburn Area'
    email: lisburnfloatingsupport@macsni.org
    location: Lisburn
    type: contact
    enabled: true
  -
    name: 'Downpatrick Area'
    email: downpatrickfloatingsupport@macsni.org
    location: Downpatrick
    type: contact
    enabled: true
  -
    name: 'Newry Area'
    email: newryfloatingsupport@macsni.org
    location: Newry
    type: contact
    enabled: true
  -
    name: 'Prison Resettlement'
    email: prisons@macsni.org
    type: contact
    enabled: true
website: 'https://www.macsni.org/'
use_meta_keywords: false
no_index_page: false
no_follow_links: false
sitemap_priority: '0.5'
sitemap_changefreq: daily
override_twitter_settings: false
override_twitter_card_settings: false
twitter_card_type_page: summary
updated_by: cb226d0f-4883-4544-9237-a7b44b67c7dc
updated_at: 1648620541
---

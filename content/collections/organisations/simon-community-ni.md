---
id: d147e532-038b-4cba-ac08-b5d0deb59c95
blueprint: organisations
title: 'Simon Community NI'
logo: logos/simon.png
target_group: 'Northern Ireland (18+)'
referral_source: 'Self-referral form on website'
replicator:
  -
    name: 'Simon Community NI'
    email: cap@simoncommunity.org
    phone_number: '0800 171 2222'
    location: 'Northern Ireland'
    type: contact
    enabled: true
website: 'https://simoncommunity.org/services/floating-support-1'
use_meta_keywords: false
no_index_page: false
no_follow_links: false
sitemap_priority: '0.5'
sitemap_changefreq: daily
override_twitter_settings: false
override_twitter_card_settings: false
twitter_card_type_page: summary
updated_by: cb226d0f-4883-4544-9237-a7b44b67c7dc
updated_at: 1648617374
---

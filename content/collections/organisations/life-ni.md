---
id: 6f4e3d0a-6a56-4811-a386-cd910b97af55
blueprint: organisations
title: 'Life Ni'
logo: logos/lifeNI.png
target_group: |-
  Pregnant women and mothers or parents of children of pre-school age.
  Greater Belfast area.
referral_source: |-
  Self-referral, Social Services, Health visiting, Family Support Hub, NIHE
  Email or phone for referral form
replicator:
  -
    name: 'Life Ni'
    email: glenysdunlop@lifecharity.org.uk
    phone_number: '07921055060'
    location: Belfast
    type: contact
    enabled: true
website: 'https://lifecharity.org.uk/'
use_meta_keywords: false
no_index_page: false
no_follow_links: false
sitemap_priority: '0.5'
sitemap_changefreq: daily
override_twitter_settings: false
override_twitter_card_settings: false
twitter_card_type_page: summary
updated_by: cb226d0f-4883-4544-9237-a7b44b67c7dc
updated_at: 1648616829
---

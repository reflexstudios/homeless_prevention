---
id: 8d1e07c8-cc73-4080-837e-c0d560c88570
blueprint: organisations
title: 'Hosford Homelessness Service East Belfast Mission'
logo: logos/HosfordHomelessnessService.png
target_group: 'Anybody in housing need in East Belfast'
referral_source: 'Self-referral. Anybody can refer themselves or organisations can refer people they support. Just phone or email asking for Tenancy Support (see phone number and email below).'
replicator:
  -
    name: 'Hosford Homelessness Service'
    email: Homelessness@ebm.org.uk
    phone_number: '028 90458560'
    location: 'East Belfast'
    type: contact
    enabled: true
website: 'https://www.ebm.org.uk/'
use_meta_keywords: false
no_index_page: false
no_follow_links: false
sitemap_priority: '0.5'
sitemap_changefreq: daily
override_twitter_settings: false
override_twitter_card_settings: false
twitter_card_type_page: summary
updated_by: cb226d0f-4883-4544-9237-a7b44b67c7dc
updated_at: 1648616399
---

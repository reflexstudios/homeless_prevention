---
id: be661345-5fad-4c4b-b86b-0794d414eceb
blueprint: organisations
title: 'Threshold services'
logo: logos/threshold.png
target_group: 'Adults within the Greater Belfast area, who are finding it difficult to maintain their tenancy and suffer from mental health problems.'
referral_source: |-
  Email Flatmgt@threshold-service.co.uk to request an information leaflet and application pack. 

  This can be completed by G.P, CMHT, N.I.H.E and self-referrals.  

  When applications are returned you will receive a letter of acknowledgement. A date will be arranged to meet to complete a risk assessment.  

  After the risk assessment is completed and it's assessed that our service suits your needs you will be placed on a waiting list.  Staff will contact you periodically to keep you informed of the waiting time.
replicator:
  -
    name: 'Rachael Moody'
    role: '07513950880'
    phone_number: '02890872255'
    type: contact
    enabled: true
website: 'https://www.threshold-services.co.uk/'
use_meta_keywords: false
no_index_page: false
no_follow_links: false
sitemap_priority: '0.5'
sitemap_changefreq: daily
override_twitter_settings: false
override_twitter_card_settings: false
twitter_card_type_page: summary
updated_by: cb226d0f-4883-4544-9237-a7b44b67c7dc
updated_at: 1648617572
---

---
id: ef394990-c4c5-4a86-a8dc-c1a111fa9853
blueprint: organisations
title: 'Action Ability Belfast'
logo: placeholders/actionAbility.png
website: 'https://www.facebook.com/Actionabilitybelfast'
accordion_content:
  -
    type: paragraph
    content:
      -
        type: text
        marks:
          -
            type: bold
        text: 'Bold title'
      -
        type: hard_break
        marks:
          -
            type: bold
      -
        type: hard_break
        marks:
          -
            type: bold
      -
        type: text
        marks:
          -
            type: italic
        text: 'New line italic '
      -
        type: hard_break
        marks:
          -
            type: italic
      -
        type: hard_break
        marks:
          -
            type: italic
      -
        type: text
        marks:
          -
            type: link
            attrs:
              href: 'statamic://entry::2a543645-79aa-4860-b670-7bd0f9e88c80'
              rel: null
              target: null
              title: null
        text: 'New line link'
      -
        type: hard_break
  -
    type: bullet_list
    content:
      -
        type: list_item
        content:
          -
            type: paragraph
            content:
              -
                type: text
                text: 'List item 1'
      -
        type: list_item
        content:
          -
            type: paragraph
            content:
              -
                type: text
                text: 'List item 2'
      -
        type: list_item
        content:
          -
            type: paragraph
            content:
              -
                type: text
                text: 'List item 3'
  -
    type: paragraph
  -
    type: set
    attrs:
      values:
        type: contact_blocks
        contact_blocks:
          -
            location: Belfast
            name: 'Contact name 1'
            email: 'Contact email 1'
            phone: '12345678987654'
            type: new_block
            enabled: true
          -
            location: Derry
            name: 'Contact name 2'
            email: 'Contact email 2'
            phone: '234567898765'
            type: new_block
            enabled: true
  -
    type: paragraph
updated_by: ad8cdc7b-f52f-4a9f-8b5a-2a06b60bb046
updated_at: 1675676662
target_group: |-
  18+ North/West Belfast including the Shankill area. 
  Physical disability, sensory disability or long term health condition.
referral_source: |-
  Self-referral
  Family or friends 
  GP
  Social Worker
  Housing Officer
  Community Worker 
  etc
replicator:
  -
    name: 'Nicky Clarke'
    email: n.clarke@usdt.co.uk
    phone_number: '02890236677'
    location: Belfast
    type: contact
    enabled: true
    role: 'FS Co-ordinator'
  -
    name: 'Paula mc Murran'
    email: paulamcmurran@usdt.co.uk
    phone_number: '02890236677'
    location: Belfast
    type: contact
    enabled: true
    role: 'AAB Programme Manager'
use_meta_keywords: false
no_index_page: false
no_follow_links: false
sitemap_priority: '0.5'
sitemap_changefreq: daily
override_twitter_settings: false
override_twitter_card_settings: false
twitter_card_type_page: summary
---

---
id: 3429af25-7b37-4bf0-b304-1c6f0d30dc80
blueprint: organisations
title: Depaul
logo: logos/Depaul-(1).png
target_group: 'Families who are homeless or who are at risk of homelessness in the greater Belfast and Lisburn area. Referrals must be over 18 years of age with children under 18 years of age as part of their household or pregnant. We can accept referrals between the age of 16 and 18 years in circumstances where social services are involved.'
referral_source: 'To refer please email: fsfloatingsupport@depaulcharity.net or call 028 95215116 to request a referral form. Families can self-refer or be referred by another voluntary/community or voluntary organisation'
replicator:
  -
    name: Depaul
    email: fsfloatingsupport@depaulcharity.net
    phone_number: '028 95215116'
    type: contact
    enabled: true
website: 'https://ie.depaulcharity.org/'
use_meta_keywords: false
no_index_page: false
no_follow_links: false
sitemap_priority: '0.5'
sitemap_changefreq: daily
override_twitter_settings: false
override_twitter_card_settings: false
twitter_card_type_page: summary
updated_by: cb226d0f-4883-4544-9237-a7b44b67c7dc
updated_at: 1648616262
---

---
id: 230bfa16-9c1f-4b72-8997-b51356d92e76
blueprint: organisations
title: 'Triangle Floating Support'
logo: logos/TriangleFloatingSupport.png
target_group: 'For those who have experienced addiction issues.'
referral_source: 'Referral agents include anyone who knows you well, including GPs, social workers, probation officers, housing officers, advice agencies, the Northern Ireland Housing Executive or any other statutory or voluntary agency.'
replicator:
  -
    name: 'Suzanne Kelly'
    email: info@trianglehousing.org.uk
    phone_number: '02890459555'
    type: contact
    enabled: true
website: 'https://www.trianglehousing.org.uk'
use_meta_keywords: false
no_index_page: false
no_follow_links: false
sitemap_priority: '0.5'
sitemap_changefreq: daily
override_twitter_settings: false
override_twitter_card_settings: false
twitter_card_type_page: summary
updated_by: cb226d0f-4883-4544-9237-a7b44b67c7dc
updated_at: 1648617644
---

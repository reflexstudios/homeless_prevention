---
id: 5eb40fef-a381-4a06-8560-cda519a1e54b
blueprint: organisations
title: 'Praxis Care'
logo: logos/praxis.png
target_group: |-
  Banbridge - People with mental health problems
  Fermanagh/Tyrone - Older people inc dementia & people with mental ill health under 65
  Strabane/Derry/Limavady - 65+ with dementia 
  Coleraine - Older people who may have diagnosis of memory problems and adults 18+ with a diagnosed mental illness, living in community independently with their own tenancy.
replicator:
  -
    name: 'Banbridge services:'
    email: susanholloway@praxiscare.org.uk
    phone_number: '02840 669453'
    location: Banbridge
    type: contact
    enabled: true
  -
    name: 'Coleraine services:'
    email: leannemcilvenny@praxiscare.org.uk
    phone_number: '02870 352292'
    location: Coleraine
    type: contact
    enabled: true
  -
    name: 'Strabane/Derry/Limavady:'
    email: traceydevenney@praxiscare.org.uk
    phone_number: '02871 308020'
    location: 'Strabane, Derry, Limavady'
    type: contact
    enabled: true
  -
    name: 'Fermanagh/Tyrone services:'
    email: siobhanwilson@praxiscare.org.uk
    phone_number: '02867 722778'
    location: 'Fermanagh, Tyrone'
    type: contact
    enabled: true
website: 'https://www.praxiscare.org'
use_meta_keywords: false
no_index_page: false
no_follow_links: false
sitemap_priority: '0.5'
sitemap_changefreq: daily
override_twitter_settings: false
override_twitter_card_settings: false
twitter_card_type_page: summary
updated_by: cb226d0f-4883-4544-9237-a7b44b67c7dc
updated_at: 1649683130
---

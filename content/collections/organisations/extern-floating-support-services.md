---
id: 0acc8250-6be4-4fcb-af64-75efda4a2465
blueprint: organisations
title: 'Extern Floating Support Services'
logo: logos/Extern.png
target_group: |-
  Refugee FS, Complex needs & Criminal justice 

  Refugee FSS - Individuals and families in Belfast who have been granted refugee status and leave to remain in the UK.

  Complex needs FSS - individuals and families in accommodation/ tenancies who are at risk of homelessness and have complex needs (Belfast)

  Criminal justice FSS - Provides support and direct interventions in the community to service users with a history of offending (Belfast, Northern Trust Area and Derry/Londonderry)

  SAIL FSS - offers support and advice to those experiencing homelessness and to those who are at risk of becoming homeless (Mid-Ulster and South Tyrone)
referral_source: 'Floating Support accept referrals from Statutory and Voluntary agencies/self-referrals.'
replicator:
  -
    name: 'Refugee & Complex needs services'
    role: 'Monday – Friday 9am-5pm'
    email: hst@extern.org
    phone_number: '02890330433'
    location: 'UK wide'
    type: contact
    enabled: true
  -
    name: 'Criminal Justice service'
    email: Theresa.ormandy@extern.org
    phone_number: '07423435371'
    type: contact
    enabled: true
  -
    name: 'SAIL FSS'
    email: mustadmin@extern.org
    phone_number: '028 8676 1586'
    location: 'Mid-Ulster and South Tyrone'
    type: contact
    enabled: true
website: 'https://www.extern.org/Pages/Category/housing-and-homeless-services'
use_meta_keywords: false
no_index_page: false
no_follow_links: false
sitemap_priority: '0.5'
sitemap_changefreq: daily
override_twitter_settings: false
override_twitter_card_settings: false
twitter_card_type_page: summary
updated_by: cb226d0f-4883-4544-9237-a7b44b67c7dc
updated_at: 1648616623
---

---
id: 2dcfb810-906d-4f36-8797-7d5af832ff31
blueprint: lesson
title: 'Defining Homelessness'
parent: 9bb8bf90-90bc-42ac-be63-79252c9d9096
template: /courses/courseDetail
updated_by: cb226d0f-4883-4544-9237-a7b44b67c7dc
updated_at: 1649174544
hero_banner: placeholders/banner3.png
training_tool_general_content:
  -
    type: set
    attrs:
      values:
        type: video
        video_length: '2 minutes'
        video_description: 'Listen to this short audio recording which describes what we understand do be a home'
        video_url: 'videos/Audio_1_Defining _Homelessness_Mar_2022.mp4'
        video_placeholder_image: placeholders/Screenshot-2022-04-05-at-16.58.02.png
lesson_length: 2
use_meta_keywords: false
no_index_page: false
no_follow_links: false
sitemap_priority: '0.5'
sitemap_changefreq: daily
override_twitter_settings: false
override_twitter_card_settings: false
twitter_card_type_page: summary
title_line_2: Homelessness
title_line_1: Defining
---

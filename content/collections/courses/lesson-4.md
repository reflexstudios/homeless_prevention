---
id: f43f3c77-6639-4213-ba34-f7fc91f7889d
blueprint: lesson
title: 'Lesson 4'
hero_banner: placeholders/banner14.png
title_line_1: Lesson
title_line_2: Four
training_tool_general_content:
  -
    type: paragraph
    content:
      -
        type: text
        text: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nunc eleifend eleifend velit, vitae lobortis augue volutpat ut. Nulla a tristique elit, varius cursus nibh. In venenatis suscipit lobortis. Etiam scelerisque venenatis neque eu fermentum. Quisque porttitor nisi diam, sit amet dapibus nulla congue vitae. Aliquam bibendum molestie maximus. Donec mauris libero, euismod non nibh non, bibendum lacinia elit. Ut id viverra mi. Phasellus pharetra luctus est non dignissim. In at massa sapien.'
  -
    type: paragraph
    content:
      -
        type: text
        text: 'Phasellus id vehicula erat, sit amet pulvinar eros. Nam suscipit auctor vestibulum. Donec in lacus in augue pulvinar dapibus. Vestibulum neque lacus, eleifend eget turpis vel, suscipit malesuada nisi. Nam pellentesque egestas augue, in porttitor velit suscipit eu. Mauris sed arcu sapien. Suspendisse sodales metus id mauris tempus porta.'
parent: 9599fab6-f88a-481f-a063-d0896f3b731e
template: /courses/courseDetail
lesson_length: 5
updated_by: 6532ca68-be39-4700-9969-cf9e09d93366
updated_at: 1669739501
---

---
id: cca31e53-76d9-485b-98d1-a7e0128964d6
blueprint: lesson
title: 'The Difference it makes'
parent: d206c780-913c-408c-9c83-ec74bdb743d7
template: /courses/courseDetail
updated_by: cb226d0f-4883-4544-9237-a7b44b67c7dc
updated_at: 1649176008
title_line_2: 'it makes'
hero_banner: placeholders/banner2.png
training_tool_general_content:
  -
    type: set
    attrs:
      values:
        type: video
        video_length: '2:53mins'
        video_description: 'Individuals share their experience of what changed when Floating Support became involved'
        video: placeholders/testVideo.mp4
        video_url: videos/Video_6_The_Difference_it_makes_Mar_2022.mp4
        video_placeholder_image: placeholders/Screenshot-2022-04-05-at-17.26.19.png
  -
    type: paragraph
title_line_1: 'The Difference'
use_meta_keywords: false
no_index_page: false
no_follow_links: false
sitemap_priority: '0.5'
sitemap_changefreq: daily
override_twitter_settings: false
override_twitter_card_settings: false
twitter_card_type_page: summary
lesson_length: 3
---

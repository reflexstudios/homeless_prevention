---
id: dcc0ca6b-7411-45a2-a150-f594280484f2
blueprint: lesson
title: 'What do we mean by homelessness prevention?'
parent: d1708531-8658-4310-acbe-6104f0c69e9d
template: /courses/courseDetail
updated_by: cb226d0f-4883-4544-9237-a7b44b67c7dc
updated_at: 1649166861
title_line_2: 'homelessness prevention?'
hero_banner: placeholders/banner12.png
training_tool_general_content:
  -
    type: paragraph
    content:
      -
        type: hard_break
      -
        type: text
        text: 'There are many ways in which homelessness can be prevented. The issue of homelessness is a complex issue that can come to any of us for a variety of reasons. '
      -
        type: text
        marks:
          -
            type: bold
        text: 'The following are just some definitions that might help understand what we mean by Homelessness prevention:'
      -
        type: hard_break
        marks:
          -
            type: bold
      -
        type: hard_break
        marks:
          -
            type: bold
  -
    type: set
    attrs:
      values:
        type: speech_bubbles
        speech_bubbles:
          -
            content:
              -
                type: paragraph
                content:
                  -
                    type: text
                    text: 'Homelessness Prevention is stopping people from becoming homeless in the first place.'
            include_bubble_tail: false
            type: new_bubble
            enabled: true
          -
            content:
              -
                type: paragraph
                content:
                  -
                    type: text
                    text: 'Homelessness prevention means '
                  -
                    type: text
                    marks:
                      -
                        type: bold
                    text: 'providing people with the ways and means'
                  -
                    type: text
                    text: ' to address their housing and other needs to avoid homelessness.'
            include_bubble_tail: false
            type: new_bubble
            enabled: true
          -
            content:
              -
                type: paragraph
                content:
                  -
                    type: text
                    text: 'Homelessness Prevention is about providing individuals and families who are at imminent risk of homelessness with '
                  -
                    type: text
                    marks:
                      -
                        type: bold
                    text: 'assistance and housing support services'
                  -
                    type: text
                    text: ' necessary to maintain their current housing; OR to find new housing to avoid becoming homeless and entering emergency hostel provision.'
            include_bubble_tail: false
            type: new_bubble
            enabled: true
  -
    type: paragraph
title_line_1: 'What do we mean by'
use_meta_keywords: false
no_index_page: false
no_follow_links: false
sitemap_priority: '0.5'
sitemap_changefreq: daily
override_twitter_settings: false
override_twitter_card_settings: false
twitter_card_type_page: summary
lesson_length: 1
---

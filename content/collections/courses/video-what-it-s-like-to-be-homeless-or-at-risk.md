---
id: 4c9a09c8-2f7c-4feb-9830-4a6817b16b28
blueprint: lesson
title: 'What its like to be homeless'
parent: 9bb8bf90-90bc-42ac-be63-79252c9d9096
template: /courses/courseDetail
updated_by: cb226d0f-4883-4544-9237-a7b44b67c7dc
updated_at: 1649175344
title_line_2: 'to be homeless'
hero_banner: placeholders/banner6.png
training_tool_general_content:
  -
    type: set
    attrs:
      values:
        type: video
        video_length: '3 miutes'
        video_description: 'Individuals share their story explaining what homelessness meant to them'
        blue: 'videos/Video 1 What it is to be homeless Mar 2022.mp4'
        hello: 'videos/Video 1 What it is to be homeless Mar 2022.mp4'
        uploadedfile: videos/Video_2_The_Homelessness_Journey_Mar_2022.mp4
        video: videos/Video_1_What_it_is_to_behomeless_Mar_2022.mp4
        video_url: videos/Video_1_What_it_is_to_behomeless_Mar_2022.mp4
        video_placeholder_image: placeholders/Screenshot-2022-04-05-at-17.15.09.png
use_meta_keywords: false
no_index_page: false
no_follow_links: false
sitemap_priority: '0.5'
sitemap_changefreq: daily
override_twitter_settings: false
override_twitter_card_settings: false
twitter_card_type_page: summary
title_line_1: 'What its like'
meta_title: 'What its like to be homeless'
lesson_length: 3
---

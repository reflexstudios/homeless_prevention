---
id: fcc6d65c-116d-475f-8ae6-c3830a4fcdfc
blueprint: lesson
title: 'What prevents homelessness?'
parent: d1708531-8658-4310-acbe-6104f0c69e9d
template: /courses/courseDetail
updated_by: cb226d0f-4883-4544-9237-a7b44b67c7dc
updated_at: 1649167589
title_line_2: 'homelessness?'
hero_banner: placeholders/banner15.png
training_tool_general_content:
  -
    type: paragraph
    content:
      -
        type: text
        marks:
          -
            type: bold
        text: 'Floating Support is arguably the MOST effective way of preventing homelessness and is considered "A vital Prevention Tool"'
      -
        type: hard_break
        marks:
          -
            type: bold
      -
        type: hard_break
        marks:
          -
            type: bold
      -
        type: text
        text: 'It has been developed to support vulnerable people who might otherwise struggle to live independently. Its main functions are:'
      -
        type: hard_break
  -
    type: set
    attrs:
      values:
        type: speech_bubbles
        speech_bubbles:
          -
            content:
              -
                type: paragraph
                content:
                  -
                    type: text
                    marks:
                      -
                        type: bold
                    text: 'To prevent the loss of secure and suitable housing,'
                  -
                    type: text
                    text: ' a key aspect of which is homelessness prevention '
            include_bubble_tail: false
            type: new_bubble
            enabled: true
          -
            content:
              -
                type: paragraph
                content:
                  -
                    type: text
                    marks:
                      -
                        type: bold
                    text: 'Resettle people into housing'
                  -
                    type: text
                    text: ' following a period of homelessness or following time thy have not been living in their own home.'
            include_bubble_tail: false
            type: new_bubble
            enabled: true
  -
    type: paragraph
title_line_1: 'What prevents'
use_meta_keywords: false
no_index_page: false
no_follow_links: false
sitemap_priority: '0.5'
sitemap_changefreq: daily
override_twitter_settings: false
override_twitter_card_settings: false
twitter_card_type_page: summary
lesson_length: 1
---

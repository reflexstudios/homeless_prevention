---
id: 894e0e24-723b-465f-9044-e5af7316e433
blueprint: lesson
title: 'Make every contact count'
parent: 05d0dc17-2668-428c-b343-84dae9a29f0b
template: /courses/courseDetail
updated_by: cb226d0f-4883-4544-9237-a7b44b67c7dc
updated_at: 1648828418
title_line_2: 'contact count'
hero_banner: placeholders/banner11.png
training_tool_general_content:
  -
    type: paragraph
    content:
      -
        type: hard_break
        marks:
          -
            type: bold
      -
        type: text
        marks:
          -
            type: bold
        text: '‘Making every contact count’ was a Government report'
      -
        type: text
        text: ' which looked at how services can be managed in a way that prevents all households, regardless of whether they are families, couples, or single people, from reaching a crisis point where they are faced with homelessness. The report recommends local agencies make sure that every contact with vulnerable individuals and families really counts.'
      -
        type: hard_break
      -
        type: hard_break
      -
        type: text
        text: 'The report notes how...'
  -
    type: set
    attrs:
      values:
        type: blockquote
        content:
          -
            type: paragraph
            content:
              -
                type: text
                marks:
                  -
                    type: bold
                text: 'For many people, becoming homeless is not the beginning of their problems;'
              -
                type: text
                text: ' it comes at the end of a long line of crises, a long line of interactions with public and voluntary sector services, a long line of missed opportunities.'
  -
    type: paragraph
    content:
      -
        type: hard_break
        marks:
          -
            type: bold
      -
        type: text
        marks:
          -
            type: bold
        text: 'We must change that.'
      -
        type: text
        text: ' To “make every contact count” each of us that may encounter vulnerable households at risk of homelessness should understand homelessness and how they can intervene to assist in its prevention.'
      -
        type: hard_break
title_line_1: 'Make every'
use_meta_keywords: false
no_index_page: false
no_follow_links: false
sitemap_priority: '0.5'
sitemap_changefreq: daily
override_twitter_settings: false
override_twitter_card_settings: false
twitter_card_type_page: summary
lesson_length: 1
---

---
id: 7f000b2b-af74-4891-983e-2cd6ebcd20f3
blueprint: lesson
title: 'The benefits of Floating Support'
parent: d206c780-913c-408c-9c83-ec74bdb743d7
template: /courses/courseDetail
updated_by: cb226d0f-4883-4544-9237-a7b44b67c7dc
updated_at: 1648828482
title_line_2: 'Floating Support'
hero_banner: placeholders/banner3.png
training_tool_general_content:
  -
    type: set
    attrs:
      values:
        type: blockquote
        content:
          -
            type: paragraph
            content:
              -
                type: text
                text: "It helps support tenants in their individual needs with things that I cannot.\_ With the right support in place it helps the tenant but also myself.\_ This may even be with simple things for example if i need to contact the customer but have been unsuccessful I can contact the support services to see if they can assist. (NIHE, Housing Advisor)"
  -
    type: set
    attrs:
      values:
        type: blockquote
        content:
          -
            type: paragraph
            content:
              -
                type: text
                text: "Floating Support is an invaluable asset to have in the community.\_ It complements and has a direct impact on not only housing related issues but also individuals health and wellbeing therefore impacting directly in a positive way on the health and social care system. (Social Worker, Homeless Support Team)"
  -
    type: set
    attrs:
      values:
        type: blockquote
        content:
          -
            type: paragraph
            content:
              -
                type: text
                text: "The right support level at the right time can make a huge difference for example helping one of our tenants during those first days changing an empty space into a cosy home made such a difference. Just helping them make better informed\_ choices, Whether it is helping to get white goods or how to connect them into services in their community like Sure Start. (Housing Association, Housing Officer)"
title_line_1: 'The benefits of'
use_meta_keywords: false
no_index_page: false
no_follow_links: false
sitemap_priority: '0.5'
sitemap_changefreq: daily
override_twitter_settings: false
override_twitter_card_settings: false
twitter_card_type_page: summary
lesson_length: 2
---

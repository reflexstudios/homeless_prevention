---
id: 1c77eb36-90af-4b8f-a82a-d75f1f4cc7e5
blueprint: courses
title: 'Homelessness Prevention Awareness Tool'
updated_by: cb226d0f-4883-4544-9237-a7b44b67c7dc
updated_at: 1649162590
course_length: 42mins
course_description:
  -
    type: paragraph
    content:
      -
        type: text
        text: 'Anyone who may '
      -
        type: text
        marks:
          -
            type: bold
        text: 'encounter vulnerable households'
      -
        type: text
        text: ' at risk of homelessness should understand homelessness and how they can intervene to assist in its prevention'
course_image: placeholders/banner12.png
use_meta_keywords: false
no_index_page: false
no_follow_links: false
sitemap_priority: '0.5'
sitemap_changefreq: daily
override_twitter_settings: false
override_twitter_card_settings: false
twitter_card_type_page: summary
---

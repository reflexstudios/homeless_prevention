---
id: d8ee2d88-b557-4dd0-8e50-d42eaff47096
blueprint: lesson
title: 'Housing Related Support Tasks'
parent: d206c780-913c-408c-9c83-ec74bdb743d7
template: /courses/courseDetail
updated_by: cb226d0f-4883-4544-9237-a7b44b67c7dc
updated_at: 1649167823
title_line_2: 'Support Tasks'
hero_banner: placeholders/banner15.png
training_tool_general_content:
  -
    type: paragraph
    content:
      -
        type: text
        text: 'Floating support provides people with housing related support, this can include:'
  -
    type: set
    attrs:
      values:
        type: speech_bubbles
        speech_bubbles:
          -
            content:
              -
                type: paragraph
                content:
                  -
                    type: text
                    text: 'Sustaining current housing, ensuring security of tenure'
            include_bubble_tail: false
            type: new_bubble
            enabled: true
          -
            content:
              -
                type: paragraph
                content:
                  -
                    type: text
                    text: 'Providing assistance to find suitable accommodation and set up home.'
            include_bubble_tail: false
            type: new_bubble
            enabled: true
          -
            content:
              -
                type: paragraph
                content:
                  -
                    type: text
                    text: 'Maximising income to meet housing costs and managing finances'
            include_bubble_tail: false
            type: new_bubble
            enabled: true
          -
            content:
              -
                type: paragraph
                content:
                  -
                    type: text
                    text: 'Assisting with access to health & social care and specialist services'
            include_bubble_tail: false
            type: new_bubble
            enabled: true
          -
            content:
              -
                type: paragraph
                content:
                  -
                    type: text
                    text: 'Support with daily domestic, life and socials skills'
            include_bubble_tail: false
            type: new_bubble
            enabled: true
          -
            content:
              -
                type: paragraph
                content:
                  -
                    type: text
                    text: 'Encourage and support positive community participation and reengage with family'
            include_bubble_tail: false
            type: new_bubble
            enabled: true
  -
    type: paragraph
title_line_1: 'Housing Related'
use_meta_keywords: false
no_index_page: false
no_follow_links: false
sitemap_priority: '0.5'
sitemap_changefreq: daily
override_twitter_settings: false
override_twitter_card_settings: false
twitter_card_type_page: summary
lesson_length: 2
---

---
id: c3b85595-09f0-4986-a31f-5a7dacfa11bc
blueprint: lesson
title: 'The Homelessness Journey'
parent: 05d0dc17-2668-428c-b343-84dae9a29f0b
template: /courses/courseDetail
updated_by: cb226d0f-4883-4544-9237-a7b44b67c7dc
updated_at: 1649175711
title_line_2: Journey
hero_banner: placeholders/banner10.png
training_tool_general_content:
  -
    type: set
    attrs:
      values:
        type: video
        video_length: '2:19'
        video_description: 'The Homelessness Journey'
        video_url: videos/Video_2_The_Homelessness_Journey_Mar_2022.mp4
        video_placeholder_image: placeholders/Screenshot-2022-04-05-at-17.21.30.png
  -
    type: paragraph
title_line_1: 'The Homelessness'
use_meta_keywords: false
no_index_page: false
no_follow_links: false
sitemap_priority: '0.5'
sitemap_changefreq: daily
override_twitter_settings: false
override_twitter_card_settings: false
twitter_card_type_page: summary
lesson_length: 3
---

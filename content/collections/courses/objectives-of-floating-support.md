---
id: c0285fed-9e9b-46a9-951b-de3f83bc9c1f
blueprint: lesson
title: 'Aims and Objectives of Floating Support'
parent: d206c780-913c-408c-9c83-ec74bdb743d7
template: /courses/courseDetail
updated_by: cb226d0f-4883-4544-9237-a7b44b67c7dc
updated_at: 1649167675
title_line_2: 'Floating Support'
hero_banner: placeholders/banner12.png
training_tool_general_content:
  -
    type: paragraph
    content:
      -
        type: text
        text: 'As stated in the last module, '
      -
        type: text
        marks:
          -
            type: bold
        text: 'Floating Support is arguably the most effective way to prevent homelessness by providing housing related support to those at risk of homelessness. '
      -
        type: text
        text: 'It can:'
  -
    type: set
    attrs:
      values:
        type: speech_bubbles
        speech_bubbles:
          -
            content:
              -
                type: paragraph
                content:
                  -
                    type: text
                    text: 'Achieve a'
                  -
                    type: text
                    marks:
                      -
                        type: bold
                    text: ' better quality of life for vulnerable people'
                  -
                    type: text
                    text: ' to live more independently and maintain their tenancies, '
            include_bubble_tail: false
            type: new_bubble
            enabled: true
          -
            content:
              -
                type: paragraph
                content:
                  -
                    type: text
                    text: 'Provide housing '
                  -
                    type: text
                    marks:
                      -
                        type: bold
                    text: 'support services to prevent problems '
                  -
                    type: text
                    text: 'that can often lead to hospitalisation, institutional care, or homelessness.'
            include_bubble_tail: false
            type: new_bubble
            enabled: true
          -
            content:
              -
                type: paragraph
                content:
                  -
                    type: text
                    text: 'Help to '
                  -
                    type: text
                    marks:
                      -
                        type: bold
                    text: 'smooth the transition to independent living'
                  -
                    type: text
                    text: ' for those leaving an institutionalised environment.'
            include_bubble_tail: false
            type: new_bubble
            enabled: true
  -
    type: paragraph
title_line_1: 'Aims and Objectives of'
use_meta_keywords: false
no_index_page: false
no_follow_links: false
sitemap_priority: '0.5'
sitemap_changefreq: daily
override_twitter_settings: false
override_twitter_card_settings: false
twitter_card_type_page: summary
lesson_length: 2
---

---
id: d427fe55-9c8a-429d-a478-e8f0e91edce7
blueprint: lesson
title: 'The role of Floating Support'
parent: d206c780-913c-408c-9c83-ec74bdb743d7
template: /courses/courseDetail
updated_by: cb226d0f-4883-4544-9237-a7b44b67c7dc
updated_at: 1649175901
title_line_2: 'Floating Support'
hero_banner: placeholders/banner14.png
training_tool_general_content:
  -
    type: set
    attrs:
      values:
        type: video
        video_length: '4:30mins'
        video_description: 'The role of Floating Support'
        video: placeholders/testVideo.mp4
        video_url: videos/Video_5_The_role_of_floating_support_Mar_2022.mp4
        video_placeholder_image: placeholders/Screenshot-2022-04-05-at-17.24.46.png
  -
    type: paragraph
title_line_1: 'The role of'
use_meta_keywords: false
no_index_page: false
no_follow_links: false
sitemap_priority: '0.5'
sitemap_changefreq: daily
override_twitter_settings: false
override_twitter_card_settings: false
twitter_card_type_page: summary
lesson_length: 5
---

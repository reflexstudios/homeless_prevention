---
id: e0378e11-a4b4-48c9-a7cc-1e1796fb3a7e
blueprint: lesson
title: 'An Introduction to our Services'
parent: 10e4b05e-1af0-49d1-ad59-473e203c0271
template: /courses/courseDetail
updated_by: cb226d0f-4883-4544-9237-a7b44b67c7dc
updated_at: 1649176081
title_line_2: 'an Introduction to our Services'
hero_banner: placeholders/banner11.png
training_tool_general_content:
  -
    type: set
    attrs:
      values:
        type: video
        video_url: 'videos/Audio_2_Floating_Support_An introductio_to_our_services_Mar_2022.mp4'
        video_length: '1:35'
        video_description: 'Introduction to our Services'
        video_placeholder_image: placeholders/Screenshot-2022-04-05-at-17.27.36.png
  -
    type: paragraph
title_line_1: 'Floating Support –'
use_meta_keywords: false
no_index_page: false
no_follow_links: false
sitemap_priority: '0.5'
sitemap_changefreq: daily
override_twitter_settings: false
override_twitter_card_settings: false
twitter_card_type_page: summary
lesson_length: 2
---

---
id: d80be049-f0a8-43c5-9f86-889e65f24041
blueprint: lesson
title: 'Points to consider'
parent: 05d0dc17-2668-428c-b343-84dae9a29f0b
template: /courses/courseDetail
updated_by: cb226d0f-4883-4544-9237-a7b44b67c7dc
updated_at: 1648828399
title_line_2: consider
hero_banner: placeholders/banner9.png
training_tool_general_content:
  -
    type: paragraph
    content:
      -
        type: text
        marks:
          -
            type: bold
        text: 'Risk factors in themselves do not definitively mean that someone would commence a homeless journey; '
      -
        type: text
        text: 'more that the likelihood of becoming homeless increased and that this in itself also increased if there were multiple risks or triggers.'
  -
    type: set
    attrs:
      values:
        type: speech_bubbles
        speech_bubbles:
          -
            content:
              -
                type: paragraph
                content:
                  -
                    type: text
                    marks:
                      -
                        type: bold
                    text: 'There is often a historical element to the homelessness'
                  -
                    type: text
                    text: ', embedded in the family unit and family circumstances'
            include_bubble_tail: false
            type: new_bubble
            enabled: true
          -
            content:
              -
                type: paragraph
                content:
                  -
                    type: text
                    text: 'The starting point of a homeless journey is '
                  -
                    type: text
                    marks:
                      -
                        type: bold
                    text: 'often as a result of a sudden and in some cases unexpected trigger'
                  -
                    type: text
                    text: ', outside of the individual’s control e.g. death of a parent'
            include_bubble_tail: false
            type: new_bubble
            enabled: true
          -
            content:
              -
                type: paragraph
                content:
                  -
                    type: text
                    marks:
                      -
                        type: bold
                    text: 'Triggers are inextricably linked to the service users other or additional needs'
                  -
                    type: text
                    text: ', including their mental and physical health and other socio-economic factors'
            include_bubble_tail: false
            type: new_bubble
            enabled: true
  -
    type: paragraph
title_line_1: 'Points to'
use_meta_keywords: false
no_index_page: false
no_follow_links: false
sitemap_priority: '0.5'
sitemap_changefreq: daily
override_twitter_settings: false
override_twitter_card_settings: false
twitter_card_type_page: summary
lesson_length: 2
---

---
id: 9cfcd2a3-ddfa-4945-9e65-43c315eac8c5
blueprint: lesson
title: 'What do we mean by ''Homelessness''?'
parent: 9bb8bf90-90bc-42ac-be63-79252c9d9096
template: /courses/courseDetail
updated_by: cb226d0f-4883-4544-9237-a7b44b67c7dc
updated_at: 1649166255
hero_banner: placeholders/banner4.png
training_tool_general_content:
  -
    type: paragraph
    content:
      -
        type: text
        text: 'It is difficult to define homelessness as it can mean so many different things to those who experience it. '
  -
    type: set
    attrs:
      values:
        type: 4_col_hover_blocks
        blocks:
          -
            icon: icons/newHouse.svg
            text: 'living in poor conditions that are damaging your health'
          -
            icon: icons/bed.svg
            text: 'staying in a hostel'
          -
            icon: icons/shouting.svg
            text: 'at risk of violence if you stay in your home'
          -
            icon: icons/group.svg
            text: 'living in very overcrowded conditions'
          -
            icon: Group-28.svg
            text: 'feeling unsafe where you live'
          -
            icon: icons/cases.svg
            text: 'staying with friends or family'
          -
            icon: icons/sleep.svg
            text: 'sleeping on the streets'
          -
            icon: icons/sadHouse.svg
            text: 'living in a house that is unsuitable for you'
        blocks_main_title: 'However You may be defined as homeless and entitled to help if you are...'
title_line_2: 'by ''Homelessness''?'
lesson_length: 2
use_meta_keywords: false
no_index_page: false
no_follow_links: false
sitemap_priority: '0.5'
sitemap_changefreq: daily
override_twitter_settings: false
override_twitter_card_settings: false
twitter_card_type_page: summary
title_line_1: 'What do we mean'
---

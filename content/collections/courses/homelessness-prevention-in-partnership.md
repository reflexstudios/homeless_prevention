---
id: 9245693b-8e0b-4a23-a285-90d1171dd63c
blueprint: lesson
title: 'Homelessness prevention in partnership'
hero_banner: placeholders/banner14.png
parent: d1708531-8658-4310-acbe-6104f0c69e9d
template: /courses/courseDetail
use_meta_keywords: false
no_index_page: false
no_follow_links: false
sitemap_priority: '0.5'
sitemap_changefreq: daily
override_twitter_settings: false
override_twitter_card_settings: false
twitter_card_type_page: summary
updated_by: cb226d0f-4883-4544-9237-a7b44b67c7dc
updated_at: 1649175835
title_line_1: Homelessness
title_line_2: 'prevention in partnership'
training_tool_general_content:
  -
    type: set
    attrs:
      values:
        type: video
        video_length: '2:51'
        video_description: 'Housing Executive’s, Housing Solutions Team explains their work in preventing homelessness and the importance of floating support and working in partnership'
        video_url: videos/Video_4_Homelessness_Prevention_in_Partnership_Mar_2022.mp4
        video_placeholder_image: placeholders/Screenshot-2022-04-05-at-17.23.34.png
lesson_length: 3
---

---
id: ea52f1d1-7836-4df4-a829-19cf640d976e
blueprint: lesson
title: Welcome
parent: 537c38c2-5a60-4d3b-8f39-13114cb9f837
updated_by: cb226d0f-4883-4544-9237-a7b44b67c7dc
updated_at: 1649162752
template: /courses/courseDetail
hero_banner: placeholders/trainingPlaceholder.png
training_tool_general_content:
  -
    type: paragraph
    content:
      -
        type: text
        text: 'This course helps equip the general public, private and social landlords as well as frontline workers to '
      -
        type: text
        marks:
          -
            type: bold
        text: 'better understand the signs to help prevent homelessness.'
lesson_length: 1
use_meta_keywords: false
no_index_page: false
no_follow_links: false
sitemap_priority: '0.5'
sitemap_changefreq: daily
override_twitter_settings: false
override_twitter_card_settings: false
twitter_card_type_page: summary
title_line_1: Welcome
---

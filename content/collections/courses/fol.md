---
id: fced6ba3-fa83-4937-b730-12d654788878
blueprint: lesson
title: 'Floating Support Services directory'
parent: 10e4b05e-1af0-49d1-ad59-473e203c0271
template: /courses/courseDetail
updated_by: cb226d0f-4883-4544-9237-a7b44b67c7dc
updated_at: 1648828493
title_line_2: 'Services directory'
hero_banner: placeholders/banner14.png
training_tool_general_content:
  -
    type: paragraph
    content:
      -
        type: text
        text: 'There are a wide range of floating support services that provide the same housing related support but have been '
      -
        type: text
        marks:
          -
            type: bold
        text: 'funded by Supporting People (NIHE) to work with specific service user groups.'
  -
    type: set
    attrs:
      values:
        type: organisations
title_line_1: 'Floating Support'
use_meta_keywords: false
no_index_page: false
no_follow_links: false
sitemap_priority: '0.5'
sitemap_changefreq: daily
override_twitter_settings: false
override_twitter_card_settings: false
twitter_card_type_page: summary
lesson_length: 2
---

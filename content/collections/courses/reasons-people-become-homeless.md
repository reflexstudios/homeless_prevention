---
id: 551bbc66-470c-457f-ae09-4ce6e227cf9a
blueprint: lesson
title: "Reasons\_people become\_homeless"
parent: 9bb8bf90-90bc-42ac-be63-79252c9d9096
template: /courses/courseDetail
updated_by: cb226d0f-4883-4544-9237-a7b44b67c7dc
updated_at: 1648828331
hero_banner: placeholders/banner5.png
title_line_2: "become\_homeless"
training_tool_general_content:
  -
    type: paragraph
    content:
      -
        type: text
        text: 'Let''s take a look at the main reasons that people become homeless in Northern Ireland'
  -
    type: paragraph
    content:
      -
        type: text
        marks:
          -
            type: bold
        text: 'Top 4 reasons'
      -
        type: text
        text: ' for homelessness'
  -
    type: set
    attrs:
      values:
        type: image
        image:
          - Reasons_graph-(1).png
  -
    type: paragraph
    content:
      -
        type: text
        marks:
          -
            type: bold
        text: 'Other historic reasons'
      -
        type: text
        text: ' for homelessness'
  -
    type: set
    attrs:
      values:
        type: speech_bubbles
        speech_bubbles:
          -
            content:
              -
                type: paragraph
                content:
                  -
                    type: text
                    text: 'Domestic Violence'
            include_bubble_tail: false
            type: new_bubble
            enabled: true
          -
            content:
              -
                type: paragraph
                content:
                  -
                    type: text
                    text: 'Mental Ill health'
            include_bubble_tail: false
            type: new_bubble
            enabled: true
          -
            content:
              -
                type: paragraph
                content:
                  -
                    type: text
                    text: 'Neighbourhood harassment'
            include_bubble_tail: false
            type: new_bubble
            enabled: true
          -
            content:
              -
                type: paragraph
                content:
                  -
                    type: text
                    text: 'Leaving Care'
            include_bubble_tail: false
            type: new_bubble
            enabled: true
          -
            content:
              -
                type: paragraph
                content:
                  -
                    type: text
                    text: 'Substance use difficulties'
            include_bubble_tail: false
            type: new_bubble
            enabled: true
lesson_length: 2
use_meta_keywords: false
no_index_page: false
no_follow_links: false
sitemap_priority: '0.5'
sitemap_changefreq: daily
override_twitter_settings: false
override_twitter_card_settings: false
twitter_card_type_page: summary
title_line_1: "Reasons\_people"
---

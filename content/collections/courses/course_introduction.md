---
id: 10619370-0cf2-4cef-8c11-af6b0f2ab6af
blueprint: lesson
title: 'Course Introduction'
hero_banner: placeholders/banner11.png
title_line_1: Course
title_line_2: Introduction
training_tool_general_content:
  -
    type: paragraph
    content:
      -
        type: text
        text: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nunc eleifend eleifend velit, vitae lobortis augue volutpat ut. Nulla a tristique elit, varius cursus nibh. In venenatis suscipit lobortis. Etiam scelerisque venenatis neque eu fermentum. Quisque porttitor nisi diam, sit amet dapibus nulla congue vitae. Aliquam bibendum molestie maximus. Donec mauris libero, euismod non nibh non, bibendum lacinia elit. Ut id viverra mi. Phasellus pharetra luctus est non dignissim. In at massa sapien.'
  -
    type: paragraph
    content:
      -
        type: text
        text: 'Phasellus id vehicula erat, sit amet pulvinar eros. Nam suscipit auctor vestibulum. Donec in lacus in augue pulvinar dapibus. Vestibulum neque lacus, eleifend eget turpis vel, suscipit malesuada nisi. Nam pellentesque egestas augue, in porttitor velit suscipit eu. Mauris sed arcu sapien. Suspendisse sodales metus id mauris tempus porta.'
  -
    type: paragraph
  -
    type: set
    attrs:
      values:
        type: 3_col_hover_blocks
        blocks_main_title: 'Title Area'
        blocks:
          -
            icon: Group-28.svg
            block_title: sdvsdvsdv
            block_content: vsdvsdvsdvsdvsdv
          -
            icon: Group-28.svg
            block_title: vsdvsdvsdv
            block_content: vsdvsdvsdvsdv
          -
            icon: Group-28.svg
            block_title: sdvsdvsdv
            block_content: sdvsdvsdvsdvsdv
  -
    type: set
    attrs:
      values:
        type: 4_col_hover_blocks
        blocks_main_title: fgnfgnfgn
        blocks:
          -
            icon: Group-28.svg
            text: fgn
          -
            icon: Group-28.svg
            text: nfgn
          -
            icon: Group-28.svg
            text: fgnfgn
          -
            icon: Group-28.svg
            text: gnfgnfgn
  -
    type: set
    attrs:
      values:
        type: audio
        audio_description: 'Audio File'
        audio: videos/Video_1_What_it_is_to_behomeless_Mar_2022.mp4
  -
    type: set
    attrs:
      values:
        type: video
        video_length: 1min
        video_description: description
        video_url: videos/Video_4_Homelessness_Prevention_in_Partnership_Mar_2022.mp4
        video_placeholder_image: placeholders/banner12.png
  -
    type: set
    attrs:
      values:
        type: image
        image:
          - image-1.png
  -
    type: set
    attrs:
      values:
        type: speech_bubbles
        speech_bubbles:
          -
            content:
              -
                type: paragraph
                content:
                  -
                    type: text
                    text: sdvsdvsdvsdvsdv
            include_bubble_tail: true
            type: new_bubble
            enabled: true
          -
            content:
              -
                type: paragraph
                content:
                  -
                    type: text
                    text: sdvsdvsdvsd
              -
                type: paragraph
                content:
                  -
                    type: text
                    text: bdfbdfbdfbsdvsd
            include_bubble_tail: true
            type: new_bubble
            enabled: true
          -
            content:
              -
                type: paragraph
                content:
                  -
                    type: text
                    text: 'ddfbdfbdfb dfbdfbdfb dfbdfbdfb'
            include_bubble_tail: true
            type: new_bubble
            enabled: true
  -
    type: paragraph
  -
    type: set
    attrs:
      values:
        type: accordion
        title: dvsdvsdvsdv
        content:
          -
            type: paragraph
            content:
              -
                type: text
                text: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nunc eleifend eleifend velit, vitae lobortis augue volutpat ut. Nulla a tristique elit, varius cursus nibh. In venenatis suscipit lobortis. Etiam scelerisque venenatis neque eu fermentum. Quisque porttitor nisi diam, sit amet dapibus nulla congue vitae. Aliquam bibendum molestie maximus. Donec mauris libero, euismod non nibh non, bibendum lacinia elit. Ut id viverra mi. Phasellus pharetra luctus est non dignissim. In at massa sapien.'
          -
            type: paragraph
            content:
              -
                type: text
                text: 'Phasellus id vehicula erat, sit amet pulvinar eros. Nam suscipit auctor vestibulum. Donec in lacus in augue pulvinar dapibus. Vestibulum neque lacus, eleifend eget turpis vel, suscipit malesuada nisi. Nam pellentesque egestas augue, in porttitor velit suscipit eu. Mauris sed arcu sapien. Suspendisse sodales metus id mauris tempus porta.'
  -
    type: set
    attrs:
      values:
        type: accordion
        title: dfbdfbdfbdfbdfbdfb
        content:
          -
            type: paragraph
            content:
              -
                type: text
                text: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nunc eleifend eleifend velit, vitae lobortis augue volutpat ut. Nulla a tristique elit, varius cursus nibh. In venenatis suscipit lobortis. Etiam scelerisque venenatis neque eu fermentum. Quisque porttitor nisi diam, sit amet dapibus nulla congue vitae. Aliquam bibendum molestie maximus. Donec mauris libero, euismod non nibh non, bibendum lacinia elit. Ut id viverra mi. Phasellus pharetra luctus est non dignissim. In at massa sapien.'
          -
            type: paragraph
            content:
              -
                type: text
                text: 'Phasellus id vehicula erat, sit amet pulvinar eros. Nam suscipit auctor vestibulum. Donec in lacus in augue pulvinar dapibus. Vestibulum neque lacus, eleifend eget turpis vel, suscipit malesuada nisi. Nam pellentesque egestas augue, in porttitor velit suscipit eu. Mauris sed arcu sapien. Suspendisse sodales metus id mauris tempus porta.'
  -
    type: set
    attrs:
      values:
        type: blockquote
        content:
          -
            type: paragraph
            content:
              -
                type: text
                text: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nunc eleifend eleifend velit, vitae lobortis augue volutpat ut. Nulla a tristique elit, varius cursus nibh. In venenatis suscipit lobortis. Etiam scelerisque venenatis neque eu fermentum. Quisque porttitor nisi diam, sit amet dapibus nulla congue vitae. Aliquam bibendum molestie maximus. Donec mauris libero, euismod non nibh non, bibendum lacinia elit. Ut id viverra mi. Phasellus pharetra luctus est non dignissim. In at massa sapien'
  -
    type: paragraph
  -
    type: paragraph
  -
    type: paragraph
  -
    type: paragraph
parent: 3f466781-3de3-491b-a982-377f49703e4d
template: /courses/courseDetail
updated_by: 6532ca68-be39-4700-9969-cf9e09d93366
updated_at: 1669740400
lesson_length: 5
---

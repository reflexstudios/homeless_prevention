---
id: 8c69792b-7f79-40d6-a81c-5aae17063dda
blueprint: lesson
title: '5 models of homelessness prevention'
parent: d1708531-8658-4310-acbe-6104f0c69e9d
template: /courses/courseDetail
updated_by: cb226d0f-4883-4544-9237-a7b44b67c7dc
updated_at: 1649167018
title_line_2: 'homelessness prevention'
hero_banner: placeholders/banner13.png
training_tool_general_content:
  -
    type: set
    attrs:
      values:
        type: 3_col_hover_blocks
        blocks:
          -
            icon: icons/handshake.svg
            block_title: Intervention
            block_content: 'either by mediation and/or counselling or through direct financial support);'
          -
            icon: icons/flowers.svg
            block_title: 'Personal Development'
            block_content: 'to provide a structured training programme to empower and equip participants identified as at risk of homelessness'
          -
            icon: icons/teaching.svg
            block_title: Training
            block_content: 'to empower and equip sector workers'
          -
            icon: icons/manWHeadrings.svg
            block_title: Awareness
            block_content: 'which seek to raise awareness among the general public, sector workers or those at risk of homelessness.'
          -
            icon: icons/social.svg
            block_title: 'Social Enterprise'
            block_content: 'to improve communities, tackle social issues and provide access to employment and skills training for individuals at risk of homelessness. '
        blocks_main_title: 'Let’s take a look at the Housing Executive’s 5 models of Homelessness Prevention'
  -
    type: paragraph
title_line_1: '5 models of'
use_meta_keywords: false
no_index_page: false
no_follow_links: false
sitemap_priority: '0.5'
sitemap_changefreq: daily
override_twitter_settings: false
override_twitter_card_settings: false
twitter_card_type_page: summary
lesson_length: 2
---

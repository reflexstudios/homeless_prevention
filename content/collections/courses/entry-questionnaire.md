---
id: a8ef0a5e-7a97-4141-967c-b8157a595b21
blueprint: lesson
title: 'Entry Questionnaire'
parent: 537c38c2-5a60-4d3b-8f39-13114cb9f837
template: /courses/courseDetail
updated_by: cb226d0f-4883-4544-9237-a7b44b67c7dc
updated_at: 1682592315
hero_banner: placeholders/banner2.png
training_tool_general_content:
  -
    type: paragraph
    content:
      -
        type: text
        marks:
          -
            type: bold
        text: 'Please take a moment to complete the questions below before moving through the training. '
      -
        type: text
        text: 'These answers will be treated anonymously and will help to improve our understanding of the training needs of the sector.'
  -
    type: set
    attrs:
      values:
        type: questionnaire
        questions:
          -
            question: 'I know the main reasons why people become homeless'
            option_choices:
              -
                option: '1'
                uuid: 8538fcc3-b449-4ba6-b68e-5940f53ba42b
              -
                option: '2'
                uuid: d58ddbe4-e5f8-4224-83ef-c57b9a809977
              -
                option: '3'
                uuid: a1fe60aa-fdcc-47c8-8de6-a97601b08728
              -
                option: '4'
                uuid: c2d0880e-5079-412e-bb62-0a5caacc5176
              -
                option: '5'
                uuid: 9dc0ff79-2498-4ae7-941c-51f20dedda2b
            uuid: a71f0966-2547-4f0e-94fa-b440cae8cce4
            label_left: Disagree
            label_right: 'Strongly Agree'
          -
            question: 'I am aware of the indicators and early signs of homelessness'
            option_choices:
              -
                option: '1'
                uuid: 5fd1e90f-3af3-482f-b2cc-317017c486d7
              -
                option: '2'
                uuid: fc23d0eb-0be5-4281-934c-34008c0d3c8b
              -
                option: '3'
                uuid: 454be902-ecbb-4fbe-aeee-cd81a7e24d77
              -
                option: '4'
                uuid: 69b27b0c-f966-4bd6-a64e-71601ed327f0
              -
                option: '5'
                uuid: 613943d3-036d-4117-823e-55c625ddc7b2
            uuid: bf79628b-6e1d-4365-b26c-168d4f7fd4ca
            label_left: Disagree
            label_right: 'Strongly Agree'
          -
            question: 'I know what Floating Support is and how it can prevent homelessness'
            option_choices:
              -
                option: '1'
                uuid: f65b4246-5f67-4fc0-8825-82977ae07663
              -
                option: '2'
                uuid: 092ea7cd-4142-45fb-ac35-d93fbd14aeae
              -
                option: '3'
                uuid: 01f3cde0-f03c-4e33-9a96-c6677ae7eecc
              -
                option: '4'
                uuid: e187c61a-9b28-4c9f-a6d5-d0437c8aada4
              -
                option: '5'
                uuid: 21519ed7-8700-4d1e-bdee-f7b92c1e55b2
            uuid: f3f7e579-2638-487e-af49-f2a05f973f2b
            label_left: Disagree
            label_right: 'Strongly Agree'
          -
            question: 'Indicate below the number of Floating Support Services you know of'
            option_choices:
              -
                option: '0'
                uuid: 1bc8ac90-dc1e-41c6-9a9e-1e65d491f91d
              -
                option: 1-2
                uuid: f19622e8-ae91-4f60-80d5-cb999376f88a
              -
                option: 3-5
                uuid: 8411c9a9-34d4-4bef-9a3e-cc5bb5453a47
              -
                option: 6-10
                uuid: 38076ba0-abf4-42cf-b25e-6cca441a2f70
              -
                option: 10+
                uuid: 9242ffff-27bc-40ea-aab6-5866e7126adb
            uuid: 55fe1dd4-b1fa-494a-950f-523d2a9f8a38
          -
            question: 'I refer to Floating Support Services when I recognise someone is at risk of homelessness'
            option_choices:
              -
                option: Never
                uuid: 507ce209-6d2f-4857-b5bd-041cf9f793cb
              -
                option: Rarely
                uuid: 930a0bce-735a-42b5-9dc3-3fc32b4c9cea
              -
                option: Sometimes
                uuid: 38e7514e-f193-494f-8ed6-4bd003e47205
              -
                option: Often
                uuid: a695309d-8404-4e23-8ba3-0be72575c164
              -
                option: 'In most cases'
                uuid: 8a948d5d-6e88-48d5-9a04-35130eab0753
            uuid: 00cfffee-6bb4-440d-8c43-4937697bb1ce
          -
            question: 'Please select the sector you are most associated with'
            option_choices:
              -
                option: 'Advice sector'
              -
                option: 'General Public'
              -
                option: Health
              -
                option: 'Housing Association'
              -
                option: 'Housing Executive'
              -
                option: Justice
              -
                option: 'Other Non stat'
              -
                option: 'Other stat'
              -
                option: 'Private Landlord'
              -
                option: 'Social care'
              -
                option: Other
            uuid: a0831390-9fe7-44bc-9931-66544f85f84f
        uuid: 936c1603-8e99-4b83-9301-9f22de8fd890
title_line_2: Questionnaire
lesson_length: 1
use_meta_keywords: false
no_index_page: false
no_follow_links: false
sitemap_priority: '0.5'
sitemap_changefreq: daily
override_twitter_settings: false
override_twitter_card_settings: false
twitter_card_type_page: summary
title_line_1: Entry
---

---
id: 82de8f9b-c472-476b-a0b2-548db9172707
blueprint: lesson
title: 'Identified risks of homelessness'
parent: 05d0dc17-2668-428c-b343-84dae9a29f0b
template: /courses/courseDetail
updated_by: cb226d0f-4883-4544-9237-a7b44b67c7dc
updated_at: 1648828390
title_line_2: 'of homelessness'
hero_banner: placeholders/banner8.png
training_tool_general_content:
  -
    type: paragraph
    content:
      -
        type: text
        text: 'Let’s take a look at some of the most common factors that may lead to homelessness'
  -
    type: paragraph
  -
    type: set
    attrs:
      values:
        type: accordion
        title: 'Family background and circumstances'
        content:
          -
            type: paragraph
            content:
              -
                type: text
                text: "This may include those with a history of the care system, those who had previous homelessness experience including family homelessness and those experiencing social or family exclusion and little or poor family support.\_"
  -
    type: set
    attrs:
      values:
        type: accordion
        title: 'Significant life event or trauma'
        content:
          -
            type: paragraph
            content:
              -
                type: text
                text: 'A significant life event or trauma, for example, miscarriage or the death of a child, sexual or domestic abuse, witnessing a traumatic event, living with a disability.'
  -
    type: set
    attrs:
      values:
        type: accordion
        title: 'Sudden loss of income or other financial change'
        content:
          -
            type: paragraph
            content:
              -
                type: text
                text: "An unexpected drop in income due to the loss of a job or change in family circumstances (e.g. reduction in benefits when a young person turns 18, reduction in earnings, death in the family of one of or the primary wage earner, retirement) And therefore they can no longer afford the outgoings they had previously committed to.\_"
  -
    type: set
    attrs:
      values:
        type: accordion
        title: 'Collision of number of smaller factors'
        content:
          -
            type: paragraph
            content:
              -
                type: text
                text: 'The start of a homeless journey is often triggered by the interplay of a number of smaller issues, rather than one large specific need or issue. For example, a young person’s behaviour within a family household, parental inability to cope with this, pressures of family finances etc. all culminating into a homeless journey.'
lesson_length: 2
title_line_1: 'Identified risks'
use_meta_keywords: false
no_index_page: false
no_follow_links: false
sitemap_priority: '0.5'
sitemap_changefreq: daily
override_twitter_settings: false
override_twitter_card_settings: false
twitter_card_type_page: summary
---

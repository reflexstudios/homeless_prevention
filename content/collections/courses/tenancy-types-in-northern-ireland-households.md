---
id: b1438317-714f-480c-934a-7ce471842d44
blueprint: lesson
title: 'Tenancy types in Northern Ireland households'
parent: 9bb8bf90-90bc-42ac-be63-79252c9d9096
template: /courses/courseDetail
updated_by: cb226d0f-4883-4544-9237-a7b44b67c7dc
updated_at: 1648828384
title_line_2: 'Ireland households'
hero_banner: placeholders/banner7.png
training_tool_general_content:
  -
    type: paragraph
    content:
      -
        type: text
        text: 'Let’s take a look at the NISRA Housing Statistics 2020-21'
  -
    type: set
    attrs:
      values:
        type: image
        image:
          - placeholders/chart.png
title_line_1: 'Tenancy types in Northern'
use_meta_keywords: false
no_index_page: false
no_follow_links: false
sitemap_priority: '0.5'
sitemap_changefreq: daily
override_twitter_settings: false
override_twitter_card_settings: false
twitter_card_type_page: summary
lesson_length: 1
---

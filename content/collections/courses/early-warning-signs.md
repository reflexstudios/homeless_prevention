---
id: 1d904bb0-c9f3-44d0-a7d0-c2f864975231
blueprint: lesson
title: 'Early warning signs'
parent: 05d0dc17-2668-428c-b343-84dae9a29f0b
template: /courses/courseDetail
updated_by: cb226d0f-4883-4544-9237-a7b44b67c7dc
updated_at: 1649166593
title_line_2: 'warning signs'
hero_banner: placeholders/banner10.png
training_tool_general_content:
  -
    type: paragraph
    content:
      -
        type: text
        text: 'As we can see there are many reasons people may identify as the main cause of their homelessness, but there are also many early signs that can start this journey towards homelessness. It is important, therefore, that we identify these early signs.'
  -
    type: set
    attrs:
      values:
        type: speech_bubbles
        speech_bubbles:
          -
            content:
              -
                type: paragraph
                content:
                  -
                    type: text
                    text: 'ASB (behavioural issues)'
            include_bubble_tail: true
            type: new_bubble
            enabled: true
          -
            content:
              -
                type: paragraph
                content:
                  -
                    type: text
                    text: 'Missed housing payments'
            include_bubble_tail: true
            type: new_bubble
            enabled: true
          -
            content:
              -
                type: paragraph
                content:
                  -
                    type: text
                    text: 'Hoarding issues'
            include_bubble_tail: true
            type: new_bubble
            enabled: true
          -
            content:
              -
                type: paragraph
                content:
                  -
                    type: text
                    text: 'Non-contact, not answering calls or letters'
            include_bubble_tail: true
            type: new_bubble
            enabled: true
          -
            content:
              -
                type: paragraph
                content:
                  -
                    type: text
                    text: 'Cultural awareness issues'
            include_bubble_tail: true
            type: new_bubble
            enabled: true
          -
            content:
              -
                type: paragraph
                content:
                  -
                    type: text
                    text: 'No visitors'
            include_bubble_tail: true
            type: new_bubble
            enabled: true
          -
            content:
              -
                type: paragraph
                content:
                  -
                    type: text
                    text: 'Neighbour complaints'
            include_bubble_tail: true
            type: new_bubble
            enabled: true
          -
            content:
              -
                type: paragraph
                content:
                  -
                    type: text
                    text: 'Upkeep of property'
            include_bubble_tail: true
            type: new_bubble
            enabled: true
          -
            content:
              -
                type: paragraph
                content:
                  -
                    type: text
                    text: 'Garden overgrown'
            include_bubble_tail: true
            type: new_bubble
            enabled: true
          -
            content:
              -
                type: paragraph
                content:
                  -
                    type: text
                    text: 'Enquiries from other services '
            include_bubble_tail: true
            type: new_bubble
            enabled: true
          -
            content:
              -
                type: paragraph
                content:
                  -
                    type: text
                    text: 'Health issues including physical and mental health disabilities'
            include_bubble_tail: true
            type: new_bubble
            enabled: true
          -
            content:
              -
                type: paragraph
                content:
                  -
                    type: text
                    text: 'Family bereavement'
            include_bubble_tail: true
            type: new_bubble
            enabled: true
          -
            content:
              -
                type: paragraph
                content:
                  -
                    type: text
                    text: 'New to area or country'
            include_bubble_tail: true
            type: new_bubble
            enabled: true
          -
            content:
              -
                type: paragraph
                content:
                  -
                    type: text
                    text: 'Damage to property'
            include_bubble_tail: true
            type: new_bubble
            enabled: true
          -
            content:
              -
                type: paragraph
                content:
                  -
                    type: text
                    text: 'Loss of employment'
            include_bubble_tail: true
            type: new_bubble
            enabled: true
          -
            content:
              -
                type: paragraph
                content:
                  -
                    type: text
                    text: 'Repeated calls from emergency service'
            include_bubble_tail: true
            type: new_bubble
            enabled: true
          -
            content:
              -
                type: paragraph
                content:
                  -
                    type: text
                    text: 'Relationship breakdown'
            include_bubble_tail: true
            type: new_bubble
            enabled: true
          -
            content:
              -
                type: paragraph
                content:
                  -
                    type: text
                    text: 'Maintenance cant get access to property'
            include_bubble_tail: true
            type: new_bubble
            enabled: true
  -
    type: paragraph
title_line_1: Early
use_meta_keywords: false
no_index_page: false
no_follow_links: false
sitemap_priority: '0.5'
sitemap_changefreq: daily
override_twitter_settings: false
override_twitter_card_settings: false
twitter_card_type_page: summary
lesson_length: 2
---

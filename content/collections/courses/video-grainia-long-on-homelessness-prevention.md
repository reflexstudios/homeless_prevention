---
id: 882d60ee-b341-4fc0-b6b3-b41885f416c9
blueprint: lesson
title: 'Homelessness Prevention'
parent: d1708531-8658-4310-acbe-6104f0c69e9d
template: /courses/courseDetail
updated_by: cb226d0f-4883-4544-9237-a7b44b67c7dc
updated_at: 1649175773
title_line_2: Prevention
hero_banner: placeholders/banner11.png
training_tool_general_content:
  -
    type: set
    attrs:
      values:
        type: video
        video_length: '2:19'
        video_description: 'Grainia Long, Chief Executive of the Housing Executive discusses the importance of Homelessness Prevention'
        video_url: videos/Video_3_Grania_Long_on_Homelessness_Prevention_Mar_2022.mp4
        video_placeholder_image: placeholders/Screenshot-2022-04-05-at-17.22.31.png
  -
    type: paragraph
title_line_1: Homelessness
use_meta_keywords: false
no_index_page: false
no_follow_links: false
sitemap_priority: '0.5'
sitemap_changefreq: daily
override_twitter_settings: false
override_twitter_card_settings: false
twitter_card_type_page: summary
lesson_length: 2
---

---
id: 0949559b-f1ec-4d71-a5dd-2bde618ad227
blueprint: pages
template: default
title: 'What is Housing Support?'
author: 01b8284c-1f33-4f0f-8644-76858e860801
updated_by: 6532ca68-be39-4700-9969-cf9e09d93366
updated_at: 1669737891
general_content:
  -
    type: set
    attrs:
      values:
        type: smaller_hero_banner
        image: placeholders/heroBannerPlaceholder.png
        main_text: 'What is Housing Support?'
        subtext_left:
          -
            type: paragraph
            content:
              -
                type: text
                text: 'Housing Support is for vulnerable people who might otherwise struggle to live independently. This is provided by a wide range of'
              -
                type: text
                marks:
                  -
                    type: bold
                text: ' Floating Support'
              -
                type: text
                text: ' Services across Northern Ireland.'
        subtext_right:
          -
            type: paragraph
            content:
              -
                type: text
                text: 'Floating Support is a vital preventative tool that supports vulnerable people who might otherwise struggle to live independently.'
  -
    type: set
    attrs:
      values:
        type: 2_col_blocks
        blocks_title: Functions
        block_subtext:
          -
            type: paragraph
            content:
              -
                type: text
                text: 'What are the main functions of Floating Support?'
        2_col_blocks:
          -
            title: Prevention
            subtext: 'To Prevent the loss of secure and suitable housing, a key aspect of which is homelessness prevention'
            link: 'entry::0949559b-f1ec-4d71-a5dd-2bde618ad227'
            type: new_block
            enabled: true
          -
            title: Resettling
            subtext: 'Resettle people into housing following a period of homelessness or following time they have not been living in their own home'
            link: 'entry::0949559b-f1ec-4d71-a5dd-2bde618ad227'
            type: new_block
            enabled: true
  -
    type: set
    attrs:
      values:
        type: 3_col_blocks
        blocks_title: Objectives
        blocks_subtext:
          -
            type: paragraph
            content:
              -
                type: text
                text: 'What are the main objectives of Floating Support Services?'
        3_col_blocks:
          -
            type: new_block
            enabled: true
            title: 'Quality of Life'
            text: 'Achieve a better quality of life for vulnerable people to live more independently and maintain their tenancies'
            link: 'entry::5c0c3ed8-6692-4350-a7ba-83a752b4accd'
          -
            title: 'Problem Prevention'
            text: 'Provide housing support services to prevent problems that can often lead to hospitalization, institutional care, or Homelessness'
            type: new_block
            enabled: true
          -
            title: 'Independent Living'
            text: 'Help to smooth the transition to independent living for those leaving an institutionalized environment'
            type: new_block
            enabled: true
  -
    type: set
    attrs:
      values:
        type: 3_col_blocks
        blocks_title: Tasks
        blocks_subtext:
          -
            type: paragraph
            content:
              -
                type: text
                text: 'What can Floating Support Service Providers do to help?'
        3_col_blocks:
          -
            icon:
              - icons/house.svg
            title: 'Security of Tenure'
            text: 'Sustaining current housing, ensuring security of tenure'
            type: new_block
            enabled: true
          -
            icon:
              - icons/chair.svg
            title: 'Setting up Home'
            text: 'Providing assistance to find suitable accommodation and “set up home”'
            type: new_block
            enabled: true
          -
            icon:
              - icons/money.svg
            title: Finances
            text: 'Maximising income to meet housing costs and managing finances'
            type: new_block
            enabled: true
          -
            icon:
              - icons/services.svg
            title: 'Access to Services'
            text: 'Assisting with access to health & social care and specialist services'
            type: new_block
            enabled: true
          -
            icon:
              - icons/hands.svg
            title: 'Domestic Support'
            text: 'Support with daily domestic life and social skills'
            type: new_block
            enabled: true
          -
            icon:
              - icons/people.svg
            title: 'Family Re-engaement'
            text: 'Encourage and support positive community participation and re-engage with family'
            type: new_block
            enabled: true
  -
    type: set
    attrs:
      values:
        type: smaller_cta
        main_text: 'Do you know the signs?'
        subtext:
          -
            type: paragraph
            content:
              -
                type: text
                text: 'Our Housing Support Training Course helps equip the general public, private and social landlords as well as frontline workers to better understand the signs and help prevent '
              -
                type: text
                marks:
                  -
                    type: bold
                text: homelessness
              -
                type: text
                text: .
        button_text: 'View Course'
        button_link: 'entry::5f1c3727-e2b7-462f-82e4-6a8e4777a228'
        image: image-1.png
meta_title: 'What is Housing Support | Homelessness Prevention Forum N.Ireland'
use_meta_keywords: false
no_index_page: false
no_follow_links: false
sitemap_priority: '0.5'
sitemap_changefreq: daily
override_twitter_settings: false
override_twitter_card_settings: false
twitter_card_type_page: summary
---

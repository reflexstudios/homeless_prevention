---
id: 2a543645-79aa-4860-b670-7bd0f9e88c80
blueprint: pages
title: News
template: news/index
author: 01b8284c-1f33-4f0f-8644-76858e860801
updated_by: 6532ca68-be39-4700-9969-cf9e09d93366
updated_at: 1669737823
general_content:
  -
    type: set
    attrs:
      values:
        type: smaller_hero_banner
        main_text: News
        subtext_left:
          -
            type: paragraph
            content:
              -
                type: text
                text: 'Stay up to date with all the '
              -
                type: text
                marks:
                  -
                    type: bold
                text: 'latest news, events and announcements'
              -
                type: text
                text: ' from the Homelessness Prevention Forum.'
  -
    type: set
    attrs:
      values:
        type: news
  -
    type: set
    attrs:
      values:
        type: smaller_cta
        main_text: 'Do you know the signs?'
        button_text: 'Visit Course'
        button_link: 'entry::5f1c3727-e2b7-462f-82e4-6a8e4777a228'
        image: placeholders/banner14.png
---

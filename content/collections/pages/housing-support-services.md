---
id: 3110311a-5b6e-4b41-ac08-bb814bb254b0
blueprint: pages
title: 'Housing Support Services'
template: organisations/index
author: 01b8284c-1f33-4f0f-8644-76858e860801
updated_by: 6532ca68-be39-4700-9969-cf9e09d93366
updated_at: 1669737991
general_content:
  -
    type: set
    attrs:
      values:
        type: smaller_hero_banner
        image: Services-Hero.png
        main_text: 'Housing support services'
        subtext_left:
          -
            type: paragraph
            content:
              -
                type: text
                text: 'There are many services available that help prevent homelessness. '
              -
                type: text
                marks:
                  -
                    type: bold
                text: 'Services are delivered across many client groups'
              -
                type: text
                text: ' - including young and older people, families, those experiencing addiction, mental health and disability issues.'
        subtext_right:
          -
            type: paragraph
            content:
              -
                type: text
                text: 'The services below are available to '
              -
                type: text
                marks:
                  -
                    type: bold
                text: everyone
              -
                type: text
                text: ' irrespective of their tenure – Housing Executive, Housing Association, private rented sector and owner occupiers.'
  -
    type: set
    attrs:
      values:
        type: organisations
  -
    type: set
    attrs:
      values:
        type: smaller_cta
        image: image-1.png
        main_text: 'Do you know the signs?'
        subtext:
          -
            type: paragraph
            content:
              -
                type: text
                text: "Our Housing Support Training Course helps equip the general public, private and social landlords as well as frontline workers to better understand the signs and help prevent\_"
              -
                type: text
                marks:
                  -
                    type: bold
                text: homelessness
              -
                type: text
                text: .
        button_text: 'Visit Course'
        button_link: 'entry::5f1c3727-e2b7-462f-82e4-6a8e4777a228'
---

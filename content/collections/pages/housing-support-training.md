---
id: 5f1c3727-e2b7-462f-82e4-6a8e4777a228
blueprint: pages
template: courses/index
title: Training
author: 01b8284c-1f33-4f0f-8644-76858e860801
updated_by: 6532ca68-be39-4700-9969-cf9e09d93366
updated_at: 1669738030
general_content:
  -
    type: set
    attrs:
      values:
        type: smaller_hero_banner
        image: Training-Listing-Hero.png
        main_text: 'Housing Support Training'
        subtext_left:
          -
            type: paragraph
            content:
              -
                type: text
                text: 'The Homelessness Prevention Forum provides'
              -
                type: text
                marks:
                  -
                    type: bold
                text: ' training to help equip the general public, private and social landlords as well as frontline workers'
              -
                type: text
                text: ' to better understand the signs and help prevent homelessness. We also provide a platform for external providers to advertise their training.'
  -
    type: set
    attrs:
      values:
        type: courses
use_meta_keywords: false
no_index_page: false
no_follow_links: false
sitemap_priority: '0.5'
sitemap_changefreq: daily
override_twitter_settings: false
override_twitter_card_settings: false
twitter_card_type_page: summary
---

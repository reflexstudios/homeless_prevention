---
id: home
blueprint: pages
title: Home
template: default
general_content:
  -
    type: set
    attrs:
      values:
        type: large_hero_banner
        image: placeholders/largeBanner.png
        main_text: 'A collective voice for housing support'
        subtext: 'We aim to equip the general public, private and social landlords as well as frontline workers to better understand the signs and help prevent homelessness.'
  -
    type: set
    attrs:
      values:
        type: multi_cta
        main_box_title: 'Housing Support Training Course'
        main_box_text: 'This course helps equip the general public, private and social landlords as well as frontline workers to better understand the signs and help prevent homelessness.'
        main_box_link: 'entry::5f1c3727-e2b7-462f-82e4-6a8e4777a228'
        small_box_1_title: 'Housing Support Services'
        small_box_1_link: 'entry::3110311a-5b6e-4b41-ac08-bb814bb254b0'
        small_box_2_title: 'What is Housing Support?'
        small_box_2_link: 'entry::0949559b-f1ec-4d71-a5dd-2bde618ad227'
        small_box_3_title: 'About Homelessness Prevention Forum'
        small_box_3_link: 'entry::5c0c3ed8-6692-4350-a7ba-83a752b4accd'
  -
    type: set
    attrs:
      values:
        type: latest_news
  -
    type: set
    attrs:
      values:
        type: large_cta
        main_text: 'we aim to help prevent homelessness through'
        link_text: 'Learn more about our mission'
        link: 'entry::5c0c3ed8-6692-4350-a7ba-83a752b4accd'
        points:
          -
            title: Awareness
            text: 'Raise awareness of services that help prevent homelessness'
            logo:
              - icons/awareness.svg
          -
            title: Access
            text: 'Provide easy access to the appropriate services'
            logo:
              - icons/access.svg
          -
            title: Communication
            text: 'Create a strong, collective voice for service providers'
            logo:
              - icons/communication.svg
          -
            title: Learning
            text: 'Sharing and learning form each other as members of the forum'
            logo:
              - icons/learning.svg
          -
            title: Response
            text: 'Ensure the voice of the service users are heard and responded to'
            logo:
              - icons/response.svg
updated_by: 6532ca68-be39-4700-9969-cf9e09d93366
updated_at: 1669737859
use_meta_keywords: false
no_index_page: false
no_follow_links: false
sitemap_priority: '0.5'
sitemap_changefreq: daily
override_twitter_settings: false
override_twitter_card_settings: false
twitter_card_type_page: summary
---
Welcome to your new Statamic website.

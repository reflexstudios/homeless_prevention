---
id: c37bf2a3-89fc-4897-b0cb-c5f98170c09e
blueprint: pages
title: Contact
author: 01b8284c-1f33-4f0f-8644-76858e860801
updated_by: 01b8284c-1f33-4f0f-8644-76858e860801
updated_at: 1644419001
general_content:
  -
    type: set
    attrs:
      values:
        type: smaller_hero_banner
        main_text: 'Get In Touch'
        subtext_left:
          -
            type: paragraph
            content:
              -
                type: text
                text: 'The Homelessness Prevention Forum is a '
              -
                type: text
                marks:
                  -
                    type: bold
                text: 'collaborative forum that is currently co-chaired by Paul Fleming & Declan Morris.'
              -
                type: text
                text: ' If you require further information about the forum please get in touch.'
  -
    type: set
    attrs:
      values:
        type: contact_blocks
        personal_contacts:
          -
            location: Belfast
            name: 'Paul Fleming'
            person_title: 'Adult Safeguarding Champion at NIACRO'
            address: '3 Amelia Street, Belfast'
            email: paul.fleming@niacro.co.uk
            type: new_person
            enabled: true
          -
            location: Derry
            name: 'Declan Morris'
            person_title: 'Project Manager Extern Homes'
            address: '3 McKinney Road, Mallusk'
            email: declan.morris@extern.org
            type: new_person
            enabled: true
  -
    type: paragraph
---

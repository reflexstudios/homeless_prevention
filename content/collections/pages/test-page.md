---
id: c41f5213-877b-43fc-b626-0cbc79f1df63
blueprint: pages
title: 'Test Page'
updated_by: 6532ca68-be39-4700-9969-cf9e09d93366
updated_at: 1669736952
general_content:
  -
    type: set
    attrs:
      enabled: false
      values:
        type: large_hero_banner
        image: placeholders/banner11.png
        main_text: 'Large Banner'
        subtext: 'Nunc consequat ipsum vitae mauris facilisis maximus. Maecenas quis turpis condimentum, mollis orci at, condimentum nisi. '
  -
    type: set
    attrs:
      values:
        type: smaller_hero_banner
        image: placeholders/banner7.png
        main_text: 'Banner Title'
        subtext_left:
          -
            type: paragraph
            content:
              -
                type: text
                text: "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nunc eleifend eleifend velit, vitae lobortis augue volutpat ut. Nulla a tristique elit, varius cursus nibh. In venenatis suscipit lobortis. Etiam scelerisque venenatis neque eu fermentum. Quisque porttitor nisi diam, sit amet dapibus nulla congue vitae.\_"
        subtext_right:
          -
            type: paragraph
            content:
              -
                type: text
                text: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. '
  -
    type: heading
    attrs:
      level: 2
    content:
      -
        type: text
        text: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. '
  -
    type: paragraph
    content:
      -
        type: text
        text: 'Nunc eleifend eleifend velit, vitae lobortis augue volutpat ut. Nulla a tristique elit, varius cursus nibh. In venenatis suscipit lobortis. '
      -
        type: text
        marks:
          -
            type: bold
        text: 'Etiam scelerisque'
      -
        type: text
        text: ' venenatis neque eu '
      -
        type: text
        marks:
          -
            type: italic
        text: fermentum
      -
        type: text
        text: '. Quisque porttitor nisi diam, sit amet dapibus nulla congue vitae. Aliquam bibendum molestie maximus. Donec mauris libero, euismod non nibh non, bibendum lacinia elit. Ut id viverra mi. Phasellus pharetra luctus est non dignissim. In at massa sapien.'
  -
    type: bullet_list
    content:
      -
        type: list_item
        content:
          -
            type: paragraph
            content:
              -
                type: text
                text: 'Phasellus id vehicula erat, sit amet pulvinar eros. '
      -
        type: list_item
        content:
          -
            type: paragraph
            content:
              -
                type: text
                text: 'Nam suscipit auctor vestibulum. '
      -
        type: list_item
        content:
          -
            type: paragraph
            content:
              -
                type: text
                text: 'Donec in lacus in augue pulvinar dapibus. '
  -
    type: paragraph
    content:
      -
        type: text
        text: 'Vestibulum neque lacus, eleifend eget turpis vel, suscipit malesuada nisi. Nam pellentesque egestas augue, in porttitor velit suscipit eu. Mauris sed arcu sapien. Suspendisse sodales metus id mauris tempus porta.'
  -
    type: blockquote
    content:
      -
        type: paragraph
        content:
          -
            type: text
            text: 'Donec blandit dolor tempus justo convallis pretium. Nam a arcu in erat aliquam molestie ac non justo. Donec dapibus sapien rhoncus ante aliquam fermentum. '
  -
    type: paragraph
    content:
      -
        type: text
        text: 'Mauris tempus sodales neque id aliquet. Praesent eu elit turpis. Donec finibus euismod sem ac ultricies. Proin facilisis mi felis, at consequat tellus pretium dictum. Mauris laoreet erat non mauris hendrerit euismod. Praesent euismod magna '
      -
        type: text
        marks:
          -
            type: link
            attrs:
              href: '#'
              rel: null
              target: null
              title: null
        text: 'vitae viverra elementum.'
      -
        type: text
        text: ' Nunc consequat ipsum vitae mauris facilisis maximus. Maecenas quis turpis condimentum, mollis orci at, condimentum nisi. Sed consectetur tempus aliquam. Integer dictum, sapien non sollicitudin tristique, te'
  -
    type: paragraph
    content:
      -
        type: image
        attrs:
          src: 'asset::assets::image-1-1669721915.png'
          alt: null
      -
        type: text
        text: 'Nunc consequat ipsum vitae mauris facilisis maximus. Maecenas quis turpis condimentum, mollis orci at, condimentum nisi. Sed consectetur tempus aliquam. Integer dictum, sapien non sollicitudin tristique, te'
  -
    type: paragraph
    content:
      -
        type: text
        text: 'Nunc consequat ipsum vitae mauris facilisis maximus. Maecenas quis turpis condimentum, mollis orci at, condimentum nisi. Sed consectetur tempus aliquam. Integer dictum, sapien non sollicitudin tristique, te'
  -
    type: set
    attrs:
      values:
        type: multi_cta
        main_box_title: Title
        main_box_text: |-
          sdvsdvsdvsdvsdvsdv

          New TExt
        main_box_link: '#'
        small_box_1_title: dsdfsdfsdf
        small_box_1_link: '#'
        small_box_2_title: sdfsdfsdfsdf
        small_box_2_link: 'entry::2a543645-79aa-4860-b670-7bd0f9e88c80'
        small_box_3_title: sdfsdfsdfsf
        small_box_3_link: '#'
  -
    type: set
    attrs:
      values:
        type: latest_news
  -
    type: set
    attrs:
      enabled: false
      values:
        type: large_cta
        main_text: 'Large CTA Titls'
        link_text: 'Link Text'
        link: '#'
        points:
          -
            title: dfbdfbdfb
            text: dfbdfbdfb
            logo:
              - icons/awareness.svg
          -
            title: fdb
            text: bdfbdfb
          -
            title: dfbdfbdfb
            text: dfbdfbdfb
  -
    type: set
    attrs:
      values:
        type: 2_col_blocks
        blocks_title: 'Title Area'
        block_subtext:
          -
            type: paragraph
            content:
              -
                type: text
                text: 'Nunc consequat ipsum vitae mauris facilisis maximus. Maecenas quis turpis condimentum, mollis orci at, condimentum nisi. Sed consectetur tempus aliquam. Integer dictum, sapien non sollicitudin tristique, te'
        2_col_blocks:
          -
            title: title
            subtext: 'Nunc consequat ipsum vitae mauris facilisis maximus. '
            type: new_block
            enabled: true
          -
            title: sdvsdvsdvsdvsdv
            subtext: 'Sed consectetur tempus aliquam. Integer dictum, sapien non sollicitudin tristique, te'
            type: new_block
            enabled: true
  -
    type: set
    attrs:
      values:
        type: 3_col_blocks
        blocks_title: '£ Col Block title'
        blocks_subtext:
          -
            type: paragraph
            content:
              -
                type: text
                text: 'dsdvdNunc consequat ipsum vitae mauris facilisis maximus. Maecenas quis turpis condimentum, mollis orci at, condimentum nisi. Sed consectetur tempus aliquam. Integer dictum, sapien non sollicitudin tristique, te'
        3_col_blocks:
          -
            title: dfbdfbdfb
            text: dfbdfbdfbdfbdf
            type: new_block
            enabled: true
          -
            title: 'b dfbdfbdfbdfb'
            text: dbfbdfbdfbdfbdfb
            type: new_block
            enabled: true
          -
            icon:
              - Group-28.svg
            title: bdfbdfbdfbdfb
            text: dfbdfbdfbdfbf
            type: new_block
            enabled: true
  -
    type: set
    attrs:
      values:
        type: text_and_image
        flip_image_and_text: false
        title: 'Title Area'
        image: image-1-1669721915.png
        text_and_image_content:
          -
            type: paragraph
            content:
              -
                type: text
                text: 'dsdvdNunc consequat ipsum vitae mauris facilisis maximus. Maecenas quis turpis condimentum, mollis orci at, condimentum nisi. Sed consectetur tempus aliquam. Integer dictum, sapien non sollicitudin tristique, tedsdvdNunc consequat ipsum vitae mauris facilisis maximus. Maecenas quis turpis condimentum, mollis orci at, condimentum nisi. Sed consectetur tempus aliquam. Integer dictum, sapien non sollicitudin tristique, te'
          -
            type: set
            attrs:
              values:
                type: blockquote
                content:
                  -
                    type: paragraph
                    content:
                      -
                        type: text
                        text: 'dsdvdNunc consequat ipsum vitae mauris facilisis maximus. Maecenas quis turpis condimentum, mollis orci at, condimentum nisi. Sed consectetur tempus aliquam. Integer dictum, sapien non sollicitudin tristique, te'
                quote_by: name
          -
            type: set
            attrs:
              values:
                type: button
                button_text: fdbdfbdfb
                button_link: '#'
  -
    type: set
    attrs:
      values:
        type: contact_blocks
        personal_contacts:
          -
            location: dsvsdv
            name: sdvsdv
            person_title: sdvsdv
            address: v
            email: sdvsdvsdvsdv
            type: new_person
            enabled: true
  -
    type: set
    attrs:
      values:
        type: smaller_cta
        image: Screenshot-2022-04-05-at-16.58.02.png
        main_text: 'Do you know the signs?'
        subtext:
          -
            type: paragraph
            content:
              -
                type: text
                text: 'Our Housing Support Training Course helps equip the general public, private and social landlords as well as frontline workers to better understand the signs and help prevent homelessness.'
        button_text: 'Visit Course 123'
        button_link: '#'
---

---
id: 5c0c3ed8-6692-4350-a7ba-83a752b4accd
blueprint: pages
title: About
author: 01b8284c-1f33-4f0f-8644-76858e860801
updated_by: 6532ca68-be39-4700-9969-cf9e09d93366
updated_at: 1669737633
general_content:
  -
    type: set
    attrs:
      values:
        type: smaller_hero_banner
        image: About-Hero-1669722105.png
        main_text: About
        subtext_left:
          -
            type: paragraph
            content:
              -
                type: text
                text: 'The Homelessness Prevention Forum was convened in 2013.  Supported by a memorandum of understanding, the forum has worked tirelessly to ensure a '
              -
                type: text
                marks:
                  -
                    type: bold
                text: 'collaborative approach to homelessness prevention'
              -
                type: text
                text: ' is at the core of housing support related services. '
        subtext_right:
          -
            type: paragraph
            content:
              -
                type: text
                text: ' '
  -
    type: set
    attrs:
      values:
        type: text_and_image
        flip_image_and_text: false
        title: 'Homelessness Prevention'
        image: image-1.png
        text_and_image_content:
          -
            type: heading
            attrs:
              level: 4
            content:
              -
                type: text
                text: 'Homelessness Prevention'
          -
            type: paragraph
            content:
              -
                type: text
                text: '‘Homelessness prevention'' means providing people with help and support to address their housing and other needs, to avoid homelessness.'
              -
                type: text
                marks:
                  -
                    type: bold
                text: ' Homelessness prevention can best be achieved by intervention at the earliest possible stage.'
          -
            type: set
            attrs:
              values:
                type: blockquote
                content:
                  -
                    type: paragraph
                    content:
                      -
                        type: text
                        text: 'For many people, becoming homeless is not the beginning of their problems; it comes at the end of a long line of crises.'
                quote_by: 'The Homelessness Strategy for Northern Ireland'
          -
            type: paragraph
            content:
              -
                type: text
                text: 'This means that by the time a household presents to the Housing Executive as homeless, many opportunities may have been missed to prevent the homelessness occurring'
          -
            type: set
            attrs:
              values:
                type: button
                button_text: 'Get in touch'
                button_link: 'entry::c37bf2a3-89fc-4897-b0cb-c5f98170c09e'
  -
    type: set
    attrs:
      values:
        type: text_and_image
        flip_image_and_text: true
        title: 'About the forum'
        image: image-2-1669721907.png
        text_and_image_content:
          -
            type: heading
            attrs:
              level: 4
            content:
              -
                type: text
                text: 'About the forum'
          -
            type: paragraph
            content:
              -
                type: text
                text: 'The Homelessness Prevention Forum was convened in 2013.  Supported by a memorandum of understanding, the forum has worked tirelessly to ensure a '
              -
                type: text
                marks:
                  -
                    type: bold
                text: 'collaborative approach to homelessness prevention'
              -
                type: text
                text: ' is at the core of housing support related services across the greater Belfast area, '
              -
                type: text
                marks:
                  -
                    type: bold
                text: 'to maximise impact, positive outcomes, and create a strong, collective voice, for all those at risk of or currently experiencing homelessness.'
              -
                type: hard_break
                marks:
                  -
                    type: bold
              -
                type: hard_break
                marks:
                  -
                    type: bold
              -
                type: text
                text: 'Meeting regularly with providers to share practice and respond to change, the Forum believes that this resource will better equip the general public, private and social landlords and frontline workers to better understand the signs that might result in homelessness and further increase their knowledge base around the housing support services that help to prevent tenancy breakdowns and, invariably, homelessness.'
              -
                type: hard_break
              -
                type: hard_break
              -
                type: text
                text: 'It will ease and increase access into their services by developing a direct pathway and connection to landlords, tenants and other homelessness services who need support.'
          -
            type: set
            attrs:
              values:
                type: button
                button_text: 'Get in touch'
                button_link: 'entry::c37bf2a3-89fc-4897-b0cb-c5f98170c09e'
          -
            type: paragraph
            content:
              -
                type: hard_break
  -
    type: set
    attrs:
      values:
        type: smaller_cta
        main_text: 'Do you know the signs?'
        button_text: 'Visit Course'
        button_link: 'entry::5f1c3727-e2b7-462f-82e4-6a8e4777a228'
        image: image-1.png
---
